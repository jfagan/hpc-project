CXX = g++
CXXFLAGS = -std=c++11 -fdiagnostics-color=auto -O2 -Wall -pedantic
DEPFLAGS = -MMD -MP
LDFLAGS = -lm

SRCDIR = src
BUILDDIR = build
INCDIR = include
BINDIR = bin
DEPDIR = .deps

SRCEXT = cc
INC = -I $(INCDIR) 
SOURCES := $(wildcard $(SRCDIR)/*.cc)
OBJECTS := $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SOURCES:.$(SRCEXT)=.o))
TARGETS := $(BINDIR)/generate $(BINDIR)/partition $(BINDIR)/isingmeasure $(BINDIR)/lattbench $(BINDIR)/autocorr $(BINDIR)/benchmark

# dependency info for existing objects
DEPS := $(OBJECTS:%.o=%.d)
DEPS := $(subst $(BUILDDIR), $(DEPDIR), $(DEPS))

# objects
GENERATE_OBJ     := $(BUILDDIR)/graph.o $(BUILDDIR)/generate.o
PARTITION_OBJ    := $(BUILDDIR)/graph.o  $(BUILDDIR)/partition.o $(BUILDDIR)/partition_main.o	
ISINGGRAPH_OBJ   := $(BUILDDIR)/graph.o $(BUILDDIR)/isingmeasurements.o $(BUILDDIR)/metropolis.o $(BUILDDIR)/worm.o $(BUILDDIR)/cluster.o
ISINGLATTICE_OBJ := $(BUILDDIR)/latticebenchmark.o $(BUILDDIR)/graph.o $(BUILDDIR)/metropolis.o
AUTOCORR_OBJ     := $(BUILDDIR)/graph.o  $(BUILDDIR)/autocorr.o $(BUILDDIR)/metropolis.o $(BUILDDIR)/worm.o $(BUILDDIR)/cluster.o
BENCHMARK_OBJ    := $(BUILDDIR)/graph.o $(BUILDDIR)/benchmark.o $(BUILDDIR)/metropolis.o

all:: $(OBJECTS) $(TARGETS)

ifneq ($(MAKECMDGOALS), clean)
 -include $(DEPS)	
 all:: MVDEPS
endif

$(BUILDDIR)/%.o: $(SRCDIR)/%.$(SRCEXT)
	$(CXX) $(CXXFLAGS) $(CPPFLAGS) $(INC) $(DEPFLAGS) -c -o $@ $<

MVDEPS:
	@mkdir -p .deps
	mv -f $(BUILDDIR)/*.d $(DEPDIR) 2>/dev/null || true

$(BINDIR)/generate: $(GENERATE_OBJ)
	$(CXX) $(CXXFLAGS) $(CPPFLAGS) $(LDFLAGS) -o $@ $^

$(BINDIR)/partition: $(PARTITION_OBJ)
	$(CXX) $(CXXFLAGS) $(CPPFLAGS) $(LDFLAGS) -o $@ $^ -L lib/primme-release-1.2.2/ -lprimme -lblas -llapack

$(BINDIR)/isingmeasure: $(ISINGGRAPH_OBJ)
	$(CXX) $(CXXFLAGS) $(CPPFLAGS) $(LDFLAGS) -o $@ $^

$(BINDIR)/lattbench: $(ISINGLATTICE_OBJ)
	$(CXX) $(CXXFLAGS) $(CPPFLAGS) $(LDFLAGS) -o $@ $^

$(BINDIR)/autocorr: $(AUTOCORR_OBJ)
	$(CXX) $(CXXFLAGS) $(CPPFLAGS) $(LDFLAGS) -o $@ $^

$(BINDIR)/benchmark: $(BENCHMARK_OBJ)
	$(CXX) $(CXXFLAGS) $(CPPFLAGS) $(LDFLAGS) -o $@ $^

# profiling - prog and args are both passed as arguments
profile: CXXFLAGS += -pg -g
profile: LDFLAGS += -pg -g
profile: $(PROG)
	./$(PROG) $(ARGS); gprof ./$(PROG) gmon.out | less

# debugging
debug: CXXFLAGS += -g -Og
debug: $(PROG)

# test target            
test:
	$(MAKE) -C unit_tests/ test

# clean    
.PHONY: clean

clean:
	$(RM) $(BUILDDIR)/* $(BINDIR)/* $(DEPDIR)/*
	$(MAKE) -C unit_tests/ clean



