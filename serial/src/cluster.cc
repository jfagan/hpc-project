
#include "cluster.h" 

#include "graph.h"
#include "mersenne.h"

#include <deque>
#include <random>
#include <vector>
 
namespace Serial {
  namespace IsingModel {

    void Cluster::initialise(Graph<site_type> &g) {

      // hot start
      for(int i=0; i < g.size(); i++) {

	g[i].value() = 1;
	if(drand48() < 0.5) // drand is ood enough here, since we're going to thermalise
	  g[i].value() = -1;
      }      
    }

    void Cluster::grow_cluster(Graph<site_type> &g, int site, char spin, const double &cluster_prob) {
      
      g[site].value() = -g[site].value();

      for(auto i: g[site].neighbours()) {
	if( g[i].value() == spin && cluster_prob > rand_double(mt()) )  { // keep the most expensive test til last!
	  grow_cluster(g, i, spin, cluster_prob);
	}
      }
    }

    void Cluster::perform_sweep(Graph<site_type> &g, const double &beta) {

      double cluster_prob = 1 - exp(-(2*beta));
      int initial_site = drand48()*g.size();
      char spin = g[initial_site].value();

      // recursive solution
      //      grow_cluster(g, initial_site, spin, cluster_prob);

      // build the cluster with a breadth first search
      ::std::deque<int> queue;
      queue.push_back(initial_site);
      g[initial_site].cluster() = true;

      while(!queue.empty()) {

      	int site = queue.front();
	
      	for(auto i: g[site].neighbours()) {
      	  if(!g[i].cluster() && g[i].value() == spin && cluster_prob > rand_double(mt()) )  { // keep the most expensive test til last!
      	    queue.push_back(i);
      	    g[i].cluster() = true;
      	  }
      	}
	
      	queue.pop_front();
      }

      // flip the cluster
      for(auto &site : g) {
      	if(site.cluster()) {
      	  site.value() = -site.value();
      	  site.cluster() = false;
      	}   
      }
    }
    
    
    double Cluster::measure_susceptibility(Graph<site_type> &g, const double &beta) {

      // int iter = 1e5;
      // int therm_iter = 1e4;

      // int n=g.size();
      // double avg_mag=0;
      // double chi=0; // susceptibility
      
      // // thermalise
      // for(int i=0; i<therm_iter; i++) {
      // 	perform_sweep(g, beta);
      // }
      
      // for(int i =0; i<iter; i++) {
      // 	perform_sweep(g, beta);
      // 	int mag=0;

      // 	for(int i=0; i<n; i++) {
      // 	  mag += g[i].value();
      // 	}

      // 	avg_mag = mag/(double)n;

      // 	chi += avg_mag*avg_mag;
      // }
      
      // return chi/iter;

      int n=g.size();
      double avg_mag=0;
      int mag=0;
      
      for(int i=0; i<n; i++) {
	mag += g[i].value();
      }

      avg_mag = mag/(double)n;
      
      return avg_mag*avg_mag - avg_mag;
    }

  }
}


