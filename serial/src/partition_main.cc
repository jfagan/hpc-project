/*
  partition_main.cc -- main function for graph partitioning
  Author       -- James Fagan (jfagan)
  Created      -- 09-07-2016
  Last updated -- Time-stamp: <2016-09-05 12:21:57 jfagan>
*/

#include "graph.h"
#include "partition.h"

#include <getopt.h>
#include <iostream>
#include <string>

using namespace std;
using Serial::Graph;

void print_usage();

int main(int argc, char **argv) 
{
  srand48(9876789);

  string filestub = "";
  bool verbose = false;
  bool rebalance = true;
  int num_partitions = 2; // must be a power of two  

  const struct option longopts[] =
    {
      {"file",        1,  0, 'f'},
      {"help",        0,  0, 'h'},
      {"verbose",     0,  0, 'v'},
      {"no-rebalance",   0,  0, 'r'},
      {"partitions",  1,  0, 'p'},
      {0,0,0,0},
    };
  
  char opt;
  int index;
  while((opt=getopt_long(argc, argv, "f:vp:rh", longopts, &index)) != -1) {
    switch(opt) {
    case 'f':
      filestub = optarg; break;
    case 'v':
      verbose=true; break;
    case 'r':
     rebalance=false; break;
    case 'p':
      num_partitions=atoi(optarg);
      if ((num_partitions == 0) || (num_partitions & (num_partitions - 1))) {
	throw std::invalid_argument("Number of partitions specified is not a power of two.");
      }
      break;
    case 'h':
      print_usage();
      exit(1);
    default:
      cerr<<"Error: invalid option"<<endl;
      exit(1);
    }
  }

  Graph<Site> g(filestub);

  partition(g, num_partitions, verbose, rebalance);
  
  if(!rebalance) {
    g.write_partition(filestub+"_unbalanced", num_partitions);
    g.write_gdf(filestub+"_unbalanced");
  } else {
    g.write_partition(filestub, num_partitions);
    g.write_gdf(filestub);
  }
}


void print_usage() {

  cout<<endl;
  cout<<" Graph Partitioning"<<endl;
  cout<<" by: James Fagan"<<endl;
  cout<<" This program partitions a graph using Fiedler's spectral partitioning algorithm. Uses PRIMME for eigenvector calculation."<<endl;
  cout<<" Usage:"<<endl;
  cout<<" ./partition [options]\n"<<endl;
  cout<<" GENERAL:"<<endl;
  cout<<"     -h, --help\n        prints this message\n"<<endl;
  cout<<"     -v, --verbose\n        activates verbose mode\n"<<endl;
  cout<<"     -f name, --file=name\n        specifies the graph file to partition\n"<<endl;
 
  cout<<" PARTITIONING:"<<endl;
  cout<<"     -p number, --partitions=value\n        chooses the number of partitions - must be a power of two (default two)\n"<<endl;
  cout<<"     -r, --no-rebalance\n        turns off heuristic rebalancing of the partition sizes (default on)\n"<<endl;

  cout<<endl;
}
