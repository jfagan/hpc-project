
// implementation of the metropolis algorithm for both Ising and Potts models

#include "metropolis.h" 

#include "graph.h"
#include "mersenne.h"

#include <random>
 
namespace Serial {
  namespace IsingModel {

    int Metropolis::agreeing_neighbours(const Graph<site_type> &g, int site) const {
      int agree=0;
      for(int i : g[site].neighbours()) {
	agree += (g[i].value() == g[site].value());
      }
      return agree;
    }


    int Metropolis::energy(const Graph<site_type> &g, int site) const {
      int agree=0;
      for(int i : g[site].neighbours()) {
	agree += (g[i].value() == g[site].value());
      }
      return agree;
    }

    void Metropolis::initialise(Graph<site_type> &g) {

      max_degree = 0;

      // hot start
      for(int i=0; i < g.size(); i++) {

	if(g[i].degree() > max_degree)
	  max_degree = g[i].degree();
	
	g[i].value() = 1;
	if(drand48() < 0.5) // drand is good enough here, since we'll thermalise
	  g[i].value() = -1;
      }

      flip_probs.resize(max_degree+1);

      for(int i=1; i<=max_degree; i++) {
	flip_probs[i].resize(i+1);
      }
    }

    void Metropolis::perform_sweep(Graph<site_type> &g, const double &beta) {

      int n = g.size();

      // generate lookup table of flip probabilities
      for(int i=1; i<=max_degree; i++) {
      	// i is the degree
      	int neighbour_sum  = -i;
      	for(int j=0; j<=i; j++) { 
      	  flip_probs[i][j] = exp(-2*neighbour_sum*beta);
      	  neighbour_sum += 2;
      	}
      }

      for(int i=0; i<n; i++) {   
	//double acceptance = exp(2*beta*( energy(g, i) ));    
	double acceptance = flip_probs[g[i].degree()][agreeing_neighbours(g,i)];

	if( acceptance > 1 || acceptance > rand_double(mt()) ) { // mersenne twister here
	  g[i].value() *= -1;
	}
      }
    }

    double Metropolis::measure_susceptibility(Graph<site_type> &g, const double &beta) {

      int n=g.size();
      double avg_mag=0;
      int mag=0;
      
      for(int i=0; i<n; i++) {
	mag += g[i].value();
      }

      avg_mag = mag/(double)n;
      
      return avg_mag*avg_mag;
    }
  }


  /********* POTTS MODEL ****************/

  namespace PottsModel {


    int Metropolis::delta_E(const Graph<site_type> &g, int site, char new_value) const {
      int agree_current=0;
      int agree_proposed=0;
      for(int i : g[site].neighbours()) {
	agree_current += (g[i].value() == g[site].value());
	agree_proposed += (g[i].value() == new_value);
      }

      return agree_proposed - agree_current;
    }

    void Metropolis::initialise(Graph<site_type> &g, int q) {

      // hot start
      for(int i=0; i < g.size(); i++) {

	g[i].value() = drand48()*q;
      }

    }

    void Metropolis::perform_sweep(Graph<site_type> &g, const double &beta, int q) {

      int n = g.size();

      for(int i=0; i<n; i++) {   
	char new_val = q*rand_double(mt());

	double acceptance = exp(2*beta*( - delta_E(g, i, new_val) ));    

	if( acceptance > rand_double(mt()) ) { // mersenne twister here
	  g[i].value() = new_val;
	}
      }
    }

  }
}
