/*
  autocorr.cc -- for measuring autocorrelations of the Ising model
  Author       -- James Fagan (jfagan)
  Created      -- 01-06-2016
  Last updated -- Time-stamp: <2016-08-31 17:29:53 jfagan>
*/

#include "cluster.h"
#include "ising.h"
#include "metropolis.h"
#include "worm.h"

#include <deque>
#include <fstream>
#include <getopt.h>
#include <iostream>
#include <sstream>
#include <string>

using namespace std;
using Serial::IsingModel::Ising;
using Serial::IsingModel::Metropolis;
using Serial::IsingModel::Worm;
using Serial::IsingModel::Cluster;

void print_usage();

template<typename Algorithm>
double autocorr_time(Ising<Algorithm> &ising, const double &beta, int n, int k, bool write_to_file);

int main(int argc, char** argv)
{
  srand48(9876789);

  string filestub="";
  double beta;
  double beta_start=0.05;
  double beta_end  =0.5;
  int measurements = 10;
  bool verbose=false;
  bool metropolis = false;
  bool cluster = false; // Wolff cluster
  int iter=1e6, terms=1e2;

  const struct option longopts[] =
    {
      {"file",            1,   0, 'f'},
      {"help",            0,   0, 'h'},
      {"verbose",         0,   0, 'v'},
      {"start",           1,   0, 's'},
      {"end",             1,   0, 'e'},
      {"measurements",    1,   0, 'm'},
      {"metropolis",      0,   0, 'M'},
      {"cluster",         0,   0, 'C'},
      {"autocorr_terms",  1,   0, 'k'},
      {"iterations",      1,   0, 'i'},
      {0,0,0,0},
    };

  char opt;
  int index;
  while((opt=getopt_long(argc, argv, "f:s:e:m:i:k:hvMC", longopts, &index)) != -1) {
    switch(opt) {
    case 'f':
      filestub = optarg; break;
    case 's':
      beta_start = atof(optarg); break;
    case 'e':
      beta_end = atof(optarg); break;
    case 'm':
      measurements = atoi(optarg); break;
    case 'h':
      print_usage(); exit(0); break;
    case 'v' :
      verbose=true; break;
    case 'i':
      iter = atoi(optarg); break;
    case 'k':
      terms = atoi(optarg); break;
    case 'M' :
      metropolis=true; break;
    case 'C' :
      cluster=true; break;
    default :
      cerr<<"Error: invalid option"<<endl;
      print_usage();
      exit(1);
    }
  }

  Ising<Metropolis> metro(filestub);
  Ising<Cluster>    wolff(filestub);

  double delta = (beta_end - beta_start)/measurements;

  if(metropolis) {
    if(verbose) cout<<"Measuring Metropolis-Hastings autocorrelations..."<<endl;
    beta = beta_start;

    string output = "../data/autocorrelation_times/M_";
    output += filestub;
    output += "_";

    ostringstream strs;
    strs << beta_start;
    strs << "_";
    strs << beta_end;
    strs << "_";
    strs << measurements;
    output +=  strs.str();      
    output += ".dat";

    ofstream out;
    out.open(output, ios::out);

    for(int i=0; i<measurements; i++) {
      beta += delta;
      if(verbose) cout<<i+1<<" of "<<measurements<<endl;

      out << i <<"\t"<< beta <<"\t" <<autocorr_time(metro, beta, iter, terms, false)<<endl;
    }
  }

  if(cluster) {
    if(verbose) cout<<"Measuring Wolff cluster autocorrelations..."<<endl;
    beta = beta_start;

    string output = "../data/autocorrelation_times/C_";
    output += filestub;
    output += "_";

    ostringstream strs;
    strs << beta_start;
    strs << "_";
    strs << beta_end;
    strs << "_";
    strs << measurements;
    output +=  strs.str();      
    output += ".dat";

    ofstream out;
    out.open(output, ios::out);

    for(int i=0; i<measurements; i++) {
      beta += delta;
      if(verbose) cout<<i+1<<" of "<<measurements<<endl;
      
      out << i <<"\t"<< beta <<"\t" <<autocorr_time(wolff, beta, iter, terms, false)<<endl;
    }
  }

  return 0;
}

template<typename Algorithm>
double autocorr_time(Ising<Algorithm> &ising, const double &beta, int iter, int terms, bool write_to_file) {
  
  vector<double> corr(terms,0.0);
  deque<double> values;
  double mean_suscept=0;

  //  ising.reset();

  // thermalise
  for(int i=0; i<1e4; i++) {
    ising.update(beta);
  }

  for(int i=0; i<terms; i++) {
    ising.update(beta);    

    values.push_front(ising.susceptibility(beta));
    mean_suscept += values.front();
  }

  for(int i=0; i<iter; i++) {
    ising.update(beta);    

    values.pop_back(); // discard oldest value
    values.push_front(ising.susceptibility(beta)); // record newest value
    mean_suscept += values.front();

    int index = 0;
    for(auto j : values) {
      corr[index++] += j*values.front();
    }
  }

  mean_suscept /= (double)(iter+terms);

  for(auto & i : corr) {
    i /= iter; 
    i -= mean_suscept*mean_suscept;
  }

  for(int i=1; i<terms; i++) {
    corr[i] /= corr[0];
  }

  corr[0]/=corr[0];

  //  double total=0;

  if(write_to_file) {
    for(auto &i: corr) {
      //  cout<<total<<endl;
      cout<<i<<endl;
      // total += i;
    }
  }

  double result = 0.5;
  
  for(auto &i : corr)
    result += i;

  return result;
}

void print_usage() {

  cout<<endl;
  cout<<" Ising model autocorrelations"<<endl;
  cout<<" by: James Fagan"<<endl;
  cout<<" This program measures the autocorrelations of a given graph using a given algorithm, over a range of beta."<<endl;
  cout<<" Usage:"<<endl;
  cout<<" ./autocorr [options]\n"<<endl;
  cout<<" GENERAL:"<<endl;
  cout<<"     -h, --help\n        prints this message\n"<<endl;
  cout<<"     -v, --verbose\n        activates verbose mode\n"<<endl;
  cout<<"     -f name, --file=name\n        specifies the graph file to run the model on\n"<<endl;
 
  cout<<" MEASUREMENTS:"<<endl;
  cout<<"     -s value, --start=value\n        sets the initial temperature (beta) to value (default 0.05)\n"<<endl;
  cout<<"     -e value, --end=value\n        sets the final temperature (beta) to value (default 1)\n"<<endl;
  cout<<"     -i value, --iterations=value\n        sets the number of iterations to average over (default 1E6)\n"<<endl;
  cout<<"     -k value, --autocorr_terms=value\n        sets how many terms to save in the Markov chain (default 1E2)\n"<<endl;
  cout<<"     -m value, --measurements=value\n        sets the number of measurements between beta_start and beta_end (default 10)\n"<<endl;

  cout<<" ALGORITHMS:"<<endl;
  cout<<"     -M, --metropolis\n        measure autocorrelations of Metropolis algorithm\n"<<endl;
  cout<<"     -C, --cluster\n        measure autocorrelations of Wolff cluster algorithm\n"<<endl;
  cout<<endl;
}
