
// implementation of the worm algorithm for the Ising model

#include "worm.h"

#include "graph.h"
#include "mersenne.h"

#include <cmath> //tanh
#include <random>

namespace Serial {
  namespace IsingModel {

    void Worm::initialise(Graph<site_type> &g) {
      // start the head and tail in the same place
      // we'll need to thermalise in the simulation
      head = drand48()*g.size();
      tail = head;
    }

    void Worm::perform_sweep(Graph<site_type> &g, const double &beta) {

      double exp_mu = 1.0/tanh(beta);

      for(int i=0; i<g.size(); i++) {

	//	if( 0.5 > drand48() )
	  move_head(g, exp_mu);
	//	else 
	//  move_tail(g, exp_mu);

	kick(g);
      }

      if(head == tail)
	coincide++;
    }

    double Worm::measure_susceptibility(Graph<site_type> &g, const double &beta) {

      int n = g.size();
      int iter = 1e5;

      // thermalise
      for(int i=0; i<iter; i++) {
       	perform_sweep(g, beta);
      }

      coincide = 0;

      // measurement
      for(int i=0; i<iter; i++) {
      	perform_sweep(g, beta);
      }

      //      std::cout<<coincide<<std::endl;

      return (double)iter/(n*coincide);
    }

    void Worm::move_head(Graph<site_type> &g, const double &exp_mu) {

      int n = g[head].neighbour(rand_double(mt())*g[head].degree());
      double acceptance;
      
      if( g[head].edge(n) ) {
	acceptance = exp_mu;
      } else {
	acceptance = 1/exp_mu;
      }

      // if(g[head].edge(n) != g[n].edge(head)) {
      // 	std::cerr<<"WARNING!"<<std::endl;
      // 	exit(1);
      // }

      if (acceptance > rand_double(mt()) ) {
	g[head].flip_link(n);
	g[n].flip_link(head);
	head = n;
      } 
    }

    void Worm::move_tail(Graph<site_type> &g, const double &exp_mu) {

      int n = g[tail].neighbour(rand_double(mt())*g[tail].degree());
      double acceptance;
      
      if( g[tail].edge(n) ) {
	acceptance = exp_mu;
      } else {
	acceptance = 1/exp_mu;
      }

      if (acceptance > rand_double(mt())) {
	g[tail].flip_link(n);
	g[n].flip_link(tail);
	tail = n;
      } 
    }
      
    void Worm::kick(Graph<site_type> &g) {
      
      if(head == tail && rand_double(mt()) > 0.5) { // kick probability of 0.5
	head = rand_double(mt())*g.size();;
	tail = head;
      }
    }

  }
}
