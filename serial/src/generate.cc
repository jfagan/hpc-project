/*
  generate.cc -- for generating graph files.
  Author       -- James Fagan (jfagan)
  Created      -- 14-07-2016
  Last updated -- Time-stamp: <2016-09-02 18:37:12 jfagan>
*/

#include "graph.h"
#include "graph_specialisations.h"

#include <fstream>
#include <getopt.h>
#include <iostream>
#include <string>

using namespace std;

void print_usage();
void make_graph(const std::string &filename, int colours, int size, int xdim, int ydim, int type, bool user);

int main(int argc, char **argv)
{
  srand48(9876789);

  string filestub = "default";
  int colour=2;
  int size=100; 
  int x=10; 
  int y=10;
  int type=1;
  bool user=false;

  const struct option longopts[] =
    {
      {"file",        1,  0, 'f'},
      {"help",        0,  0, 'h'},
      {"user-input",  0,  0, 'u'},
      {"colours",     1,  0, 'c'},
      {"rows",        1,  0, 'x'},
      {"cols",        1,  0, 'y'},
      {"type",        1,  0, 't'},
      {"size",        1,  0, 'n'},
      {0,0,0,0},
    };

  char opt;
  int index;
  while((opt=getopt_long(argc, argv, "f:c:n:x:y:t:uh", longopts, &index)) != -1) {
    switch(opt) {
    case 'f':
      filestub = optarg; break;
    case 'c':
      colour = atoi(optarg); break;
    case 'n':
      size = atoi(optarg); break;
    case 'x':
      x = atoi(optarg); break;
    case 'y':
      y = atoi(optarg); break;
    case 't':
      type = atoi(optarg); break;
    case 'u':
      user = true; break;
    case 'h':
      print_usage();
      exit(0);
    default :
      cerr<<"Error: invalid option"<<endl;
      exit(1);
    }
  }

  make_graph(filestub, colour, size, x, y, type, user);
}

void print_usage() {
  
  cout<<endl;
  cout<<" Graph Generation"<<endl;
  cout<<" by: James Fagan"<<endl;
  cout<<" This program generates an instance of a user-chosen graph type."<<endl;
  cout<<" Usage:"<<endl;
  cout<<" ./generate [options]\n"<<endl;
  cout<<" GENERAL:"<<endl;
  cout<<"     -h, --help\n        prints this message\n"<<endl;
  cout<<"     -u, --user-input\n        activates prompted input mode\n"<<endl;
  cout<<"     -f name, --file=name\n        specifies the name of the graph to be generated\n"<<endl;
 
  cout<<" GENERATION:"<<endl;
  cout<<"     -n value, --size=value\n        indicate size of random/ random local graph (default 100)\n"<<endl;
  cout<<"     -c value, --colours=value\n        indicate number of colours for random/ random local graph (default 2)\n"<<endl;
  cout<<"     -x/y value, --rows/cols=value\n        indicate number of rows/cols for grid, or for grid used to build uniform spanning tree (default 10 x 10)\n"<<endl;

}

void make_graph(const std::string &filename, int colours, int size, int xdim, int ydim, int type, bool user) {

  if(user) {
    std::cout<<"\n Graph types:"<<std::endl;
    std::cout<<"    1) tree"<<std::endl;
    std::cout<<"    2) toroidal grid"<<std::endl;
    std::cout<<"    3) random (Erdos-Reyni)"<<std::endl;
    std::cout<<"    4) random (preferential local connections)"<<std::endl;
    std::cout<<"\n Enter choice: ";

    std::cin>>type;
    std::cout<<std::endl;

    std::cout<<"\n Creating graph in "<<filename<<std::endl;
  }

  Serial::Graph<Site> g(1);

  switch(type) {
  case 1:
    if(user) {
      std::cout<<" Provide x and y dimensions of the grid used to generate the uniform spanning tree (x*y = n):"<<std::endl;
      std::cout<<"    x: ";
      std::cin>>xdim;
      std::cout<<"    y: ";
      std::cin>>ydim;
    }
    g = Serial::TreeGraph<Site>(xdim,ydim);
    break;
  case 2:
    if(user) {
      std::cout<<" Provide x and y dimensions of the grid (x*y = n):"<<std::endl;
      std::cout<<"    x: ";
      std::cin>>xdim;
      std::cout<<"    y: ";
      std::cin>>ydim;
    }
    g = Serial::Grid2D<Site>(xdim, ydim);
    break;
  case 3:
    if(user) {
      std::cout<<" Enter a graph size: ";
      std::cin>>size;
      std::cout<<" Choose number of colours: ";
      std::cin>>colours;
    }
    g = Serial::RandomGraph<Site>(colours, size);
    break;
  case 4:
    if(user) {
      std::cout<<" Enter a graph size: ";
      std::cin>>size;
      std::cout<<" Choose number of colours: ";
      std::cin>>colours;
    }
    g = Serial::RandomLocalGraph<Site>(colours, size);
    break;
  default :
    std::cerr<<"Invalid graph type.\n\n"<<std::endl;
    return;
  }

  g.write_adj(filename);
}
