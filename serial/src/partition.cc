/*
  partition.cc -- implementation of graph partitioning
  Author       -- James Fagan (jfagan)
  Created      -- 09-07-2016
  Last updated -- Time-stamp: <2016-08-17 12:25:56 jfagan>
*/

#include "partition.h"

#include "graph.h"

#include "PRIMME/primme.h"

#include <iostream>
#include <set> 
#include <vector>


using Serial::Graph;

static void print_partition_stats  (const Graph<Site> &g, int num_partitions);
static void rebalance_partition    (      Graph<Site> &g, int num_partitions);
static void grow_small_partition   (      Graph<Site> &g, std::vector<int> &partition_sizes, int target_partition, const double &delta);
static void shrink_large_partition (      Graph<Site> &g, std::vector<int> &partition_sizes, int target_partition, const double &delta);

// matrix multiplication function for PRIMME - A*x = y
void laplacian_matvec(void *x, void *y, int *blocksize, primme_params *primme) {

  Graph<Site> g = *(Graph<Site>*)(primme->matrix);
  double *src = (double *)(x);
  double *res = (double *)(y);

  for(int i=0; i<primme->n; i++) {
    res[i] = g[i].degree()*src[i];

    for(auto j: g[i].neighbours()) {
      res[i] -= src[j];
    }
  }

  y = res;
}


void partition(Graph<Site> &g, int num_partitions, bool verbose, bool rebalance) {

  int n = g.size();
  int num_eigenvecs = 1 + log2(num_partitions);

  double *eigenvalues  = new double[num_eigenvecs];
  double *eigenvectors = new double[num_eigenvecs * n];
  double *res_norms    = new double[num_eigenvecs];

  primme_params primme;
  primme_initialize(&primme);
  primme.matrixMatvec = laplacian_matvec;
  primme.matrix = (void *)(&g);
  primme.n = n;
  primme.numEvals = num_eigenvecs;

  primme_set_method(DEFAULT_MIN_TIME, &primme); // best method when A*x is cheap, as we have
  
  dprimme(eigenvalues, eigenvectors, res_norms, &primme);
       
  // colour the nodes according to the signs of corresponding entries in the first few eigenvectors
  for(int i=1; i<num_eigenvecs; i++) {
    for(int j=0; j<n; j++) {
      g[j].partition() += (eigenvectors[i*n + j] < 0) ? 1<<(i-1) : 0;
    }
  }

  if(verbose) {
    std::cout<<"Before rebalancing: "<<std::endl;
    print_partition_stats(g,num_partitions);
    std::cout<<std::endl;
  }

  if(rebalance) {

    // // rebalance the partition sizes if they aren't close enough
    rebalance_partition(g, num_partitions);  

    // print results and partition statistics
    if(verbose) {
      std::cout<<"After rebalancing: "<<std::endl;
      print_partition_stats(g,num_partitions);
    }

  }

  primme_Free(&primme);
  delete[] eigenvalues;
  delete[] eigenvectors;
  delete[] res_norms;
}


static void rebalance_partition(Graph<Site> &g, int num_partitions) {

  // calculate the partition sizes
  std::vector<int> partition_sizes(num_partitions, 0);
  for(int i=0; i<g.size(); i++) {
    partition_sizes[g[i].partition()]++;
  }

  // calculate the average size and specify the allowed tolerance
  double average_partition = (double)g.size()/(double)num_partitions;
  int delta = 0.05*average_partition; // acceptable deviation from the average (number of sites)
  

  bool balanced = false;
  int iter=0;

  // while there are sizes outside the acceptable range  
  while(!balanced) {

    balanced = true;
    if(iter++>50) break;
    
    // loop over all the partitions, checking if they have to be grown or shrunk
    for(int i=0; i<num_partitions; i++) {
      
      if( partition_sizes[i]-average_partition > delta) {
	balanced = false;
	shrink_large_partition(g, partition_sizes, i, delta);
	} else if( partition_sizes[i]-average_partition < -delta) {
	balanced = false;
	grow_small_partition(g, partition_sizes, i, delta);	
      }      
    }
  }
}

// this function allows a small partition to take sites from its neighbouring partitions
static void grow_small_partition(Graph<Site> &g, std::vector<int> &partition_sizes, int target_partition, const double &delta) {

  int n = g.size();
  int num_partitions = partition_sizes.size();

  // find the maximum degree - we don't want to go loooking for sites with a degree higher than this
  int max_degree = 0;
  for(int i=0; i<n; i++) {
    max_degree = (g[i].degree() > max_degree) ? g[i].degree() : max_degree;
  }

  double average_partition = (double)n/(double)num_partitions;

  // deal with partitions that don't have any nodes
  // we "seed" them with a node from the partition which is currently the largest 
  if(partition_sizes[target_partition] == 0) {
    int largest_partition = max_element(partition_sizes.begin(), partition_sizes.end()) - partition_sizes.begin();
  
    partition_sizes[largest_partition]--;
    partition_sizes[target_partition]++;
    
    for(int i=0; i<n; i++) {
      if(g[i].partition() == largest_partition) {
	g[i].partition() = target_partition;
	break;
      }
    }
  }
        
  // we only accept new sites to our partition that add less or equal to "threshold" cut edges
  // if we can't find any, we increase the threshold
  // after we find some we reset the threshold to one
  int threshold = 1;

  // while the partition size is outside the acceptable range
  while(partition_sizes[target_partition] < average_partition-delta) {

    // keep track of sites we're going to take over
    std::set<int> change_list;

    // loop over each site's neighbours as potential candidates
    for(int i=0; i<n; i++) {
      if(g[i].partition() == target_partition ) {
	for(auto &j : g[i].neighbours() ) {
	  if(g[j].partition() != target_partition) {

	    int total_cut = 0;

	    // preferentially change sites with less cut edges
	    for(auto &k : g[j].neighbours()) {
	      if(g[k].partition() != target_partition)
		total_cut++;
	    }

	    // record index for switching later, if we don't cut too many edges, and the partition we're taking from is larger
	    if(total_cut <= threshold && partition_sizes[g[j].partition()] > partition_sizes[target_partition]) {
	      change_list.insert(j);
	    }
	  }
	}
      }
    }
  
    // if we havent found any sites to take, increase the cut edge threshold for the next sweep
    if(change_list.size() == 0) {
      threshold++;
    } else { // reset the threshold back to 1
      threshold=1;
    }

    // if this happens, there are no more sites we can take
    if(threshold > max_degree) {
      return;
    }

    // take the sites that have been earmarked, making sure to keep track of partition sizes
    for(auto i : change_list) {
      partition_sizes[target_partition]++;
      partition_sizes[g[i].partition()]--;
      g[i].partition() = target_partition;
    } 
  }
}

// this function is similar, where nopw we have a large partition gifting sites to its neighbours
static void shrink_large_partition(Graph<Site> &g, std::vector<int> &partition_sizes, int target_partition, const double &delta) {

  int n = g.size();
  int num_partitions = partition_sizes.size();

  // determine max degree - don't want to search for sits with higher degree than this 
  int max_degree = 0;
  for(int i=0; i<n; i++) {
    max_degree = (g[i].degree() > max_degree) ? g[i].degree() : max_degree;
  }

  double average_partition = (double)n/(double)num_partitions;

  // threshold serves same purpose as in grow_small_partition
  int threshold = 1;

  while(partition_sizes[target_partition] > average_partition+delta) {

    // keep track of the changes, and whether we found any
    std::vector< std::set<int> > change_list(num_partitions);
    bool successful_change = false;

    // this part is slightly different - loop over all sites, check the neighbours to see where we can gift it to
    for(int i=0; i<n; i++) {
      if(g[i].partition() == target_partition ) {
	for(auto &j : g[i].neighbours() ) {
	  if(g[j].partition() != target_partition) {

	    int total_cut = 0;

	    // preferentially change sites with less cut edges
	    for(auto &k : g[i].neighbours()) {
	      if(g[k].partition() != g[j].partition())
		total_cut++;
	    }

	    // record index for switching later, if appropriate
	    if(total_cut <= threshold &&  partition_sizes[g[j].partition()] < partition_sizes[target_partition]) {
	      change_list[g[j].partition()].insert(i);
	      successful_change = true;
	      break;
	    }
	  }
	}
      }
    }
  
    if(!successful_change)
      threshold++;
    else 
      threshold=1;

    if(threshold > max_degree) {
      return;
    }

    std::set<int> already_changed;
    // need this in case we try to pass the same site to two different partitions
    // this messes up our tracking of the sizes

    for(int i=0; i<num_partitions; i++) {
      for(auto j : change_list[i]) {
	if(already_changed.count(j)) {
	  continue;
	} else {
	  already_changed.insert(j);
	  partition_sizes[i]++;
	  partition_sizes[target_partition]--;
	  g[j].partition() = i;
	}
      }
    } 
  }
}

static void print_partition_stats(const Graph<Site> &g, const int num_partitions) {

  std::cout<<"Connected... ";

  if( is_connected(g) ) {
    std::cout<<"yes"<<std::endl;
  } else {
    std::cout<<"no"<<std::endl;
  }
      
  // count cut edges, total and to each other partition
  int total_cut = 0;
  int total_edges = 0;
  std::vector< std::vector<int> > connectivity(num_partitions, std::vector<int>(num_partitions, 0));
  std::vector<int> internal_edges(num_partitions, 0);
  std::vector<int> partition_sizes(num_partitions, 0);

  for(int i=0; i<g.size(); i++) {

    partition_sizes[g[i].partition()]++;

    total_edges += g[i].degree();

    for(auto &j : g[i].neighbours()) {
	
      //	total_edges++;
	
      connectivity[g[i].partition()][g[j].partition()]++;
	
      if( g[i].partition() != g[j].partition()) {
	total_cut++; 
      }
    }
  }

  for(int i=0; i<num_partitions; i++) {
    connectivity[i][i] /= 2;
  }
  
  std::cout<<"Partition sizes: "<<std::endl;
  for(int i=0; i<num_partitions; i++) {
    std::cout<<"    "<<i<<": "<<partition_sizes[i]<<std::endl;
  }

  std::cout<<"Total edges: "<<total_edges/2<<std::endl;
  std::cout<<"Total cut edges: "<<total_cut/2<<std::endl;

  std::cout<<"Percentage cut: "<<100*(double)total_cut/total_edges<<"%"<<std::endl;
}
