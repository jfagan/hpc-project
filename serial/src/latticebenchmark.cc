/*
  latticebenchmark.cc -- Main program for 2-d lattice ising model
  Author       -- James Fagan (jfagan)
  Created      -- 01-06-2016
  Last updated -- Time-stamp: <2016-09-04 16:13:10 jfagan>
*/

#include "graph.h"
#include "graph_specialisations.h"
#include "ising.h"
#include "isinglattice.h"
#include "metropolis.h"

#include <getopt.h>
#include <iostream>
#include <chrono>

using namespace std;

void print_usage();

int main(int argc, char **argv)
{
  srand48(9876789);

  int size = 50;
  double beta = 1;
  int iter = 1E5;
  bool verbose=false;

  char opt;
  while((opt=getopt(argc, argv, "s:b:i:hv")) != -1) {
    switch(opt) {
    case 's':
      size=atoi(optarg); break;
    case 'b':
      beta = atof(optarg); break;
    case 'i':
      iter=atoi(optarg); break;
    case 'h':
      print_usage(); exit(0); break;
    case 'v':
      verbose=true; break;
    default :
      cerr<<"Error: invalid option"<<endl;
      print_usage();
      exit(1);
    }
  }

  Serial::Grid2D< MetropolisSite<char> > g(size, size);

  Serial::Lattice::   Ising<char> lattice(size);
  Serial::IsingModel::Ising<Serial::IsingModel::Metropolis> graph(g);

  auto start_lattice = chrono::steady_clock::now();
  
  for(int i=0; i<iter; i++) {
    lattice.update(beta);
  }

  auto end_lattice = chrono::steady_clock::now();
    
  // Store the time difference between start and end
  auto start_graph = chrono::steady_clock::now();
  
  for(int i=0; i<iter; i++) {
    graph.update(beta);
  }

  auto end_graph = chrono::steady_clock::now();

  auto diff_lattice = (end_lattice - start_lattice)/1000;     
  auto diff_graph = (end_graph - start_graph)/1000;

  if(verbose) {

    cout<<"Lattice version took : ";
    cout << chrono::duration <double, milli> (diff_lattice).count() << " ms" << endl;

    cout<<"Graph version took : ";
    cout << chrono::duration <double, milli> (diff_graph).count() << " ms" << endl;

  } else {

    cout << chrono::duration <double, milli> (diff_lattice).count() << "\t";
    cout << chrono::duration <double, milli> (diff_graph).count() << endl;

  }

}

void print_usage() {

  cout<<endl;
  cout<<" Ising model graph version VS lattice version"<<endl;
  cout<<" by: James Fagan"<<endl;
  cout<<" This program compares the speed of a 2D lattice Metropolis Ising model implemented on a graph, to that implemented on a hardcoded 2D lattice"<<endl;
  cout<<" Usage:"<<endl;
  cout<<" ./lattbench [options]\n"<<endl;
  cout<<" GENERAL:"<<endl;
  cout<<"     -h, --help\n        prints this message\n"<<endl;
  cout<<"     -v, --verbose\n        activates verbose mode\n"<<endl;
 
  cout<<" MEASUREMENTS:"<<endl;
  cout<<"     -s value, --size=value\n        sets the lattice side length to value (default 50)\n"<<endl;
  cout<<"     -i value, --iterations=value\n        sets the number of iterations to benchmark over (default 1E5)\n"<<endl;
  cout<<"     -b value, --beta=value\n        sets the inverse temperature, beta (default 1)\n"<<endl;

  cout<<endl;
}
