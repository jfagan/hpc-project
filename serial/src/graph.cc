// implementation of serial graph functionality

#include "graph.h"

#include <deque>
#include <iostream>
#include <vector>

using Serial::Graph;

void print_adjacency(const Graph<Site> &g) {
  for(int i=0; i<g.size(); i++) {

    std::cout<< i <<": ";
    for(int j : g[i].neighbours()) {
      std::cout<< j << ", ";
    }
    std::cout<<std::endl;
  }
  std::cout<<std::endl;
}

void print_laplacian(const Graph<Site> &g) {
  
  int n=g.size();

  std::cout<<"[";

  for(int i=0; i<n; i++) {
    for(int j=0; j<i; j++) {
      if(g[i].is_neighbour(j))
	std::cout<<"-1 ";
      else
	std::cout<<"0 ";
    }

    std::cout<<g[i].degree()<<" ";

    for(int j=i+1; j<n; j++) {
      if(g[i].is_neighbour(j))
	std::cout<<"-1 ";
      else
	std::cout<<"0 ";
    }
    if(i==n-1) break;
    std::cout<<";"<<std::endl;
  }

  std::cout<<"]"<<std::endl;
}

bool is_connected(const Graph<Site> &g) {

  // try to visit each site with a breadth first search, starting from zero
 
  std::deque<int> queue;
  std::vector<int> visited(g.size(), false);

  queue.push_back(0); // starting point
  visited[0] = true;

  int total_visited = 1;

  while( !queue.empty() ) {

    for(auto i : g[queue.front()].neighbours()) {
      if(!visited[i]) {
	queue.push_back(i);	
	visited[i] = true;
	total_visited++;
      }
    }
    queue.pop_front();
  }

  if(total_visited != g.size()) // graph not connected - try again
    return false;

  return true;
}

void colour_bipartite(Graph<Site> &g) {
  // use a breadth first search
  std::deque<int> queue;
  std::vector<bool> visited(g.size(), false);
  std::vector<int> level(g.size(), 0);

  g[0].colour() = 0;
  queue.push_back(0);
  visited[0] = true;
  level[0] = 0;

  while( !queue.empty() ) {

    for(auto i : g[queue.front()].neighbours()) {
      if(!visited[i]) {
	visited[i] = true;
	level[i] = level[queue.front()] +1;
	g[i].colour() = level[i]%2;
	queue.push_back(i);	
      }
    }

    queue.pop_front();
  }
}
