/*
  isingmeasurements.cc -- main program for serial Ising model on a graph
  Author       -- James Fagan (jfagan)
  Created      -- 01-06-2016
  Last updated -- Time-stamp: <2016-09-02 18:33:09 jfagan>
*/

#include "cluster.h"
#include "ising.h"
#include "metropolis.h"
#include "worm.h"

#include <getopt.h>
#include <iostream>

using namespace std;
using Serial::IsingModel::Metropolis;
using Serial::IsingModel::Worm;
using Serial::IsingModel::Cluster;
using Serial::IsingModel::Ising;

void print_usage();

int main(int argc, char** argv)
{
  srand48(9876789);

  string filestub="";
  double beta;
  double beta_start=0.05;
  double beta_end  = 1;
  int measurements = 10;
  int iter = 1e5;
  bool verbose=false;
  bool metropolis = false;
  bool worm = false;
  bool cluster = false; // Wolff cluster

  const struct option longopts[] =
    {
      {"file",         1,   0, 'f'},
      {"help",         0,   0, 'h'},
      {"verbose",      0,   0, 'v'},
      {"start",        1,   0, 's'},
      {"end",          1,   0, 'e'},
      {"measurements", 1,   0, 'm'},
      {"iterations",   1,   0, 's'},
      {"metropolis",   0,   0, 'M'},
      {"worm",         0,   0, 'W'},
      {"cluster",      0,   0, 'C'},
      {0,0,0,0},
    };

  char opt;
  int index;
  while((opt=getopt_long(argc, argv, "f:s:e:i:m:hvMWC", longopts, &index)) != -1) {
    switch(opt) {
    case 'f':
      filestub = optarg; break;
    case 's':
      beta_start = atof(optarg); break;
    case 'e':
      beta_end = atof(optarg); break;
    case 'm':
      measurements = atoi(optarg); break;
    case 'i':
      iter = atoi(optarg); break;
    case 'h':
      print_usage(); exit(0); break;
    case 'v' :
      verbose=true; break;
    case 'M' :
      metropolis=true; break;
    case 'W' :
      worm=true; break;
    case 'C' :
      cluster=true; break;
    default :
      cerr<<"Error: invalid option"<<endl;
      print_usage();
      exit(1);
    }
  }

  if(!metropolis && !worm && !cluster) {
    cout<<"\n To run a simulation, an algorithm/ algorithms must be specified using command line flags. Please see help message."<<endl;
    print_usage();
  }

  Ising<Metropolis> metro_ising (filestub);
  Ising<Worm>       worm_ising  (filestub);
  Ising<Cluster>    wolff_ising (filestub);

  beta = beta_start;
  double delta = (beta_end - beta_start)/measurements;

  if(metropolis) {
    if(verbose) cout<<"Taking Metropolis measurements..."<<endl;

    for(int i=0; i<measurements; i++) {
      beta += delta;
      if(verbose) cout<<i+1<<" of "<<measurements<<endl;

      for(int j=0; j<0.1*iter; j++)
  	metro_ising.update(beta);

      double result = 0.0;

      for(int j=0; j<iter; j++) {
  	result += metro_ising.susceptibility(beta);
  	metro_ising.update(beta);
      }
     
      cout<< i<<"\t"<< beta <<"\t"<< result/iter <<endl;
    }
  }

  if(worm) {
    if(verbose) cout<<"Taking Worm measurements..."<<endl;

    beta = beta_start;

    for(int i=0; i<measurements; i++) {
      beta += delta;
      if(verbose) cout<<i+1<<" of "<<measurements<<endl;

      // pass iter to worm ising?
      
      cout<< i<<"\t"<< beta <<"\t"<< worm_ising.susceptibility(beta) <<endl;
    }
  } 

  if(cluster) {
    if(verbose) cout<<"Taking Wolff cluster measurements..."<<endl;

    beta = beta_start;

    for(int i=0; i<measurements; i++) {
      beta += delta;
      if(verbose) cout<<i+1<<" of "<<measurements<<endl;

      for(int j=0; j<0.1*iter; j++)
  	wolff_ising.update(beta);

      double result = 0.0;

      for(int j=0; j<iter; j++) {
  	result += wolff_ising.susceptibility(beta);
  	wolff_ising.update(beta);
      }
     
      cout<< i<<"\t"<< beta <<"\t"<< result/iter <<endl;
    }
  } 

  return 0;
}


void print_usage() {

  cout<<endl;
  cout<<" Ising model on a graph"<<endl;
  cout<<" by: James Fagan"<<endl;
  cout<<" This program implements the Ising model on a graph structure, with a user-chosen algorithm."<<endl;
  cout<<" Usage:"<<endl;
  cout<<" ./isinggraph [options]\n"<<endl;
  cout<<" GENERAL:"<<endl;
  cout<<"     -h, --help\n        prints this message\n"<<endl;
  cout<<"     -v, --verbose\n        activates verbose mode\n"<<endl;
  cout<<"     -f name, --file=name\n        specifies the graph file to run the model on\n"<<endl;
 
  cout<<" MEASUREMENTS:"<<endl;
  cout<<"     -s value, --start=value\n        sets the initial temperature (beta) to value (default 0.05)\n"<<endl;
  cout<<"     -e value, --end=value\n        sets the final temperature (beta) to value (default 1)\n"<<endl;
  cout<<"     -i value, --iterations=value\n        sets the number of iterations to average over (default 1E5)\n"<<endl;
  cout<<"     -m value, --measurements=value\n        sets the number of measurements between beta_start and beta_end (default 10)\n"<<endl;

  cout<<" ALGORITHMS:"<<endl;
  cout<<"     -M, --metropolis\n        takes measurements using Metropolis algorithm\n"<<endl;
  cout<<"     -W, --worm\n        takes measurements using worm algorithm\n"<<endl;
  cout<<"     -C, --cluster\n        takes measurements using Wolff cluster algorithm\n"<<endl;
  cout<<endl;
}
