/*
  main.cc -- main program for serial Ising model on a graph
  Author       -- James Fagan (jfagan)
  Created      -- 01-06-2016
  Last updated -- Time-stamp: <2016-08-31 17:33:37 jfagan>
*/

#include "ising.h"
#include "metropolis.h"

#include <getopt.h>
#include <iostream>
#include <sys/time.h>

using namespace std;
using Serial::IsingModel::Metropolis;
using Serial::IsingModel::Ising;

void print_usage();

int main(int argc, char** argv)
{
  srand48(9876789);

  string filestub="";
  double beta = 1;
  int iter = 1e5;
 
  const struct option longopts[] =
    {
      {"file",         1,   0, 'f'},
      {"help",         0,   0, 'h'},
      {"beta",         1,   0, 'e'},
      {"iterations",   1,   0, 's'},
       {0,0,0,0},
    };

  char opt;
  int index;
  while((opt=getopt_long(argc, argv, "f:b:i:h", longopts, &index)) != -1) {
    switch(opt) {
    case 'f':
      filestub = optarg; break;
    case 'b':
      beta = atof(optarg); break;
    case 'i':
      iter = atoi(optarg); break;
    case 'h':
      print_usage(); exit(0); break;
    default :
      cerr<<"Error: invalid option"<<endl;
      print_usage();
      exit(1);
    }
  }

  struct timeval tim;
  gettimeofday(&tim, NULL);
  double t1=tim.tv_sec+(tim.tv_usec/1000000.0);
  
  Ising<Metropolis> metro_ising (filestub);
  
  for(int j=0; j<iter; j++) {
    metro_ising.update(beta);
  }
  
  gettimeofday(&tim, NULL);
  double t2=tim.tv_sec+(tim.tv_usec/1000000.0);
  
  cout<<t2-t1<<endl;
  
  return 0;
}


void print_usage() {

  cout<<endl;
  cout<<" Ising model benchmarking"<<endl;
  cout<<" by: James Fagan"<<endl;
  cout<<" This program times a given number of Metropolis updates on a user-chosen graph"<<endl;
  cout<<" Usage:"<<endl;
  cout<<" ./benchmark [options]\n"<<endl;
  cout<<" GENERAL:"<<endl;
  cout<<"     -h, --help\n        prints this message\n"<<endl;
  cout<<"     -f name, --file=name\n        specifies the graph file to run the model on\n"<<endl;
 
  cout<<" MEASUREMENTS:"<<endl;
  cout<<"     -b value, --beta=value\n        sets the temperature (beta) to value (default 1)\n"<<endl;
  cout<<"     -i value, --iterations=value\n        sets the number of iterations to run for (default 1E5)\n"<<endl;

  cout<<endl;
}
