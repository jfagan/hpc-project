/*
  infracstructure for serial Ising model on a graph
*/

#ifndef __ISINGGRAPH_H__
#define __ISINGGRAPH_H__

#include "graph.h"
#include <string>

namespace Serial {
  namespace IsingModel {
    template<typename Algorithm>
    class Ising : private Algorithm {

      typedef typename Algorithm::site_type site_type;
      using Algorithm::initialise;
      using Algorithm::perform_sweep;
      using Algorithm::measure_susceptibility;

      Graph<site_type> host;

    public:
      Ising(const Graph<site_type> &g);
    
      // file constructor
      Ising(::std::string &filestub);

      inline const int size  ()         const { return host.size(); }

      inline const Graph<site_type> &graph() const { return host; } // used for testing

      inline void reset() { initialise(host); }
      inline void update(const double &beta) { perform_sweep(host, beta); }
      inline double susceptibility(const double &beta) { return measure_susceptibility(host, beta); }
    };

#include "tpp/ising.tpp"
  }
}

#endif
