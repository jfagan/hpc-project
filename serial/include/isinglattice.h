
/*
  Ising model on a square lattice
*/

#ifndef __ISINGLATTICE_H__
#define __ISINGLATTICE_H__

#include <vector>

// for isinglattice.tpp
#include "mersenne.h"
#include <iostream>
#include <random>

namespace Serial {
namespace Lattice {

  template<typename T>
  class Ising{
    int side_length;
    std::vector<T> spins;
    std::uniform_real_distribution<double> rand_double;

  public:
    Ising(int n);

    inline T &operator[]      (int a)       { return spins[a]; }
    inline const T operator[] (int a) const { return spins[a]; }

    inline T &operator()      (int a, int b)       { return spins[a*side_length + b]; }
    inline const T operator() (int a, int b) const { return spins[a*side_length + b]; }

    inline const int size() const { return side_length*side_length; }
    inline const int side() const { return side_length; }

    void update(const double &beta);
  };
} 
}

  template<typename T>
  std::ostream& operator<< (std::ostream &strm, const Serial::Lattice::Ising<T> &lattice);

 

  template<typename T>
  double magnetisation(const Serial::Lattice::Ising<T> &lattice);

#include "tpp/isinglattice.tpp"

#endif
