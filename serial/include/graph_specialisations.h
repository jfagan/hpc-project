/*
  graph_specialisations.h -- specialisations of the basic graph class
  Author       -- James Fagan (jfagan)
  Created      -- 11-08-2016
  Last updated -- Time-stamp: <2016-08-12 17:42:26 jfagan>
*/

#ifndef __GRAPH_SPECIALISATIONS_H__
#define __GRAPH_SPECIALISATIONS_H__

#include "graph.h"

// for graph_specialisations.tpp
#include "mersenne.h"
#include <deque>
#include <list>
#include <random>

namespace Serial {

  template<typename site_type>
  class Grid2D : public Graph<site_type> {
    int grid_rows;
    int grid_cols;

  public:
    Grid2D(int x, int y);

    Grid2D(const int x, const int y, bool empty) : Graph<site_type>(2) {}
  
    // access numbers of rows and columns
    inline const int rows() const { return grid_rows; }
    inline const int cols() const { return grid_cols; }
  };

  template <typename site_type>
  std::ostream& operator<< (std::ostream& strm, const Grid2D<site_type> &grid);

  template<typename site_type>
  class TreeGraph : public Graph<site_type> {
  public:
    TreeGraph(int x, int y);
  };

  template<typename site_type>
  class RandomGraph : public Graph<site_type> {
  public:
    RandomGraph(int colours, int size);
  };

  template<typename site_type>
  class RandomLocalGraph : public Graph<site_type> {
  public:
    RandomLocalGraph(int colours, int size);
  };
  
#include "tpp/graph_specialisations.tpp"

}

#endif
