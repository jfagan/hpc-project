/*
  An arbitary graph class with spins at it's nodes, which can be further specialised
*/

#ifndef __GRAPH_H__
#define __GRAPH_H__

#include <vector>
#include "site.h"

// for graph.tpp
#include <iostream>
#include <fstream>
#include <sstream>
#include <exception>
#include <stdexcept>
#include <cstddef>
#include <string>

namespace Serial {

  template<typename site_type>
  class Graph {
    
    int total_colours;

  typedef typename std::vector<site_type>::iterator iterator;
  typedef typename std::vector<site_type>::const_iterator const_iterator;

  protected:
    // child classes need access to the sites vector too
    std::vector< site_type > sites;
 
  public:
    // empty graph with just number of colours - used to write child class constructors
    Graph(int colours) : total_colours( colours ) {}

    // graph with no edges, only coloured sites
    Graph(int colours, int size);

    // copy constructor
    Graph(const Graph<site_type> &g);

    // file constructor
    Graph(const std::string &filestub);
 
    void write_adj       (const std::string &filestub);
    void write_gdf       (const std::string &filestub); // for gephi
    void write_partition (const std::string &filestub, int partitions);

    // overloaded [] operator to access sites
    inline const site_type &operator[] (const int x) const { return sites[x]; }
    inline site_type       &operator[] (const int x)       { return sites[x]; }

    const Graph<site_type>& operator=(const Graph<site_type> &g);

    // access properties of the graph and it's sites
    inline const int size   () const { return sites.size();  }
    inline const int colours() const { return total_colours; } 
 
    // change the size of the graph
    inline void resize (int i) { sites.resize(i); }

    inline void clear_edges() { 
      for(auto &site : sites)
	site.clear();
    }

    // add a new neighbour to a site
    inline void add_link(int site, int neigh) {
      sites[ site ] .add_neigh( neigh );
      sites[ neigh ].add_neigh( site  ); // need both since graph is undirected
    }

    inline void remove_link(int site, int neigh) {
      sites[ site ] .del_neigh( neigh );
      sites[ neigh ].del_neigh( site  ); // need both since graph is undirected
    }

    // non-const iterators over sites
    inline iterator begin() { return sites.begin(); }
    inline iterator end()   { return sites.end();   }
    
    // const iterators over sites
    inline const_iterator cbegin() const { return sites.cbegin(); }
    inline const_iterator cend()   const { return sites.cend();   }
  }; 

#include "tpp/graph.tpp"

}

void print_adjacency  ( const Serial::Graph<Site> &g ); // print the graph's neighbour list
void print_laplacian  ( const Serial::Graph<Site> &g ); // print the graph's Laplacian matrix
bool is_connected     ( const Serial::Graph<Site> &g ); // check connectivity using a BFS
void colour_bipartite (       Serial::Graph<Site> &g ); // colour a graph with two colours using a BFS

#endif
