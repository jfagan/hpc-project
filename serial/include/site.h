/*
  A class to organise the metadata at each node of a graph
*/

#ifndef __SITE_H__
#define __SITE_H__

#include <vector>
#include <exception>
#include <stdexcept>
#include <algorithm>
    
class Site {

protected:
  int col; // colour
  int part; // partition 
  std::vector<int> neigh; // neighbours

  typedef typename std::vector<int>::iterator iterator;
  typedef typename std::vector<int>::const_iterator const_iterator;
      
public:

  Site() :
    col{0}, part{0} {}
  
  Site(const std::vector<int> &n) :
    col{0}, part{0}, neigh{n} {}

  void operator=(const Site &n) {
    col   = n.colour();
    part  = n.partition();
    neigh = n.neighbours();
  }

  inline const int colour() const { return col; }
  inline int      &colour()       { return col; }

  inline const int partition() const { return part; }
  inline int      &partition()       { return part; }

  // site properties
  inline const int               degree    ()      const { return neigh.size(); }
  inline const int               neighbour (int i) const { return neigh[i]; }
  inline const std::vector<int> &neighbours()      const { return neigh; }

  // check if a certain site is a neighbour
  inline const bool is_neighbour(int i) const {
    return ( std::find( neigh.cbegin(), neigh.cend(), i) != neigh.cend() );
  }

  // add a neighbour
  virtual inline void add_neigh(int site_label) {
    if(!is_neighbour( site_label ) ) {
      neigh.push_back( site_label );
    }
  }

  // remove a neighbour
  virtual inline void del_neigh(int site_label) {
    iterator position = find( neigh.begin(), neigh.end(), site_label);
    if( position == neigh.end() ) {
      throw std::out_of_range("Trying to remove non-existent neighbour!");
    } else {
      neigh.erase( position ); 
    }
  }

  virtual inline void clear() { neigh.clear(); }

  // non-const iterators over neighbours
  inline iterator begin() { return neigh.begin(); }
  inline iterator end()   { return neigh.end();   }

  // const iterators over neighbours
  inline const_iterator cbegin() const { return neigh.cbegin(); }
  inline const_iterator cend()   const { return neigh.cend();   }
};
  
#endif

