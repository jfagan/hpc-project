
#ifndef __METROPOLIS_H__
#define __METROPOLIS_H__

#include "site.h"
#include <vector>
#include <random>
    
template<typename T>
class MetropolisSite : public Site {

  T val; // value (spin)

public:
  MetropolisSite() :
    Site::Site{}, val{0} {}
  
  MetropolisSite(const std::vector<int> &n) :
    Site::Site{n}, val{0} {}

  inline void operator=(const MetropolisSite<T> &n) {
    Site::operator=(n);
    val   = n.value();
  }

  // private member access
  inline const T value() const { return val; }
  inline T&      value()       { return val; }
};

namespace Serial {

  template<typename site_type>
    class Graph;

  namespace IsingModel {

    class Metropolis {
      int max_degree;
      std::vector< std::vector<double> > flip_probs;
      std::uniform_real_distribution<double> rand_double;
      int energy (const ::Serial::Graph< MetropolisSite<char> > &g, int site) const;
      int agreeing_neighbours (const ::Serial::Graph< MetropolisSite<char> > &g, int site) const;

    protected:

      Metropolis() : rand_double{0.0, 1.0} {}

      typedef MetropolisSite<char> site_type;

      void   initialise             (::Serial::Graph<site_type> &g);
      void   perform_sweep          (::Serial::Graph<site_type> &g, const double &beta);
      double measure_susceptibility (::Serial::Graph<site_type> &g, const double &beta);
    };
  }


  namespace PottsModel {

    class Metropolis {
      std::uniform_real_distribution<double> rand_double;
      int delta_E (const ::Serial::Graph< MetropolisSite<char> > &g, int site, char value) const;

    protected:

      Metropolis() : rand_double{0.0, 1.0} {}

      typedef MetropolisSite<char> site_type;

      void   initialise             (::Serial::Graph<site_type> &g, int q);
      void   perform_sweep          (::Serial::Graph<site_type> &g, const double &beta, int q);
      double measure_susceptibility (::Serial::Graph<site_type> &g, const double &beta, int q);
    };
  }
}

#endif
