
#ifndef __CLUSTER_H__
#define __CLUSTER_H__

#include "site.h"
#include <vector>
    
template<typename T>
class ClusterSite : public Site {

  T val; // value (spin)
  bool in_cluster; // is the site in the current cluster

public:
  ClusterSite() :
    Site::Site{}, val{0} {}
  
  ClusterSite(const std::vector<int> &n) :
    Site::Site{n}, val{0} {}

  inline void operator=(const ClusterSite<T> &n) {
    Site::operator=(n);
    val   = n.value();
  }

  // private member access
  inline const T value() const { return val; }
  inline T&      value()       { return val; }  

  inline const bool cluster() const { return in_cluster; }
  inline bool&      cluster()       { return in_cluster; }  
};

namespace Serial {

  template<typename site_type>
    class Graph;

  namespace IsingModel {

    class Cluster {
      std::uniform_real_distribution<double> rand_double;
      void grow_cluster(::Serial::Graph< ClusterSite<char> > &g, int site, char spin, const double &cluster_prob);

    protected:

      Cluster() : rand_double{0.0, 1.0} {}

      typedef ClusterSite<char> site_type;

      void   initialise             (::Serial::Graph<site_type> &g);
      void   perform_sweep          (::Serial::Graph<site_type> &g, const double &beta);
      double measure_susceptibility (::Serial::Graph<site_type> &g, const double &beta);
    };
  }
}

#endif
