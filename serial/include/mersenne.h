/*
  mersenne.h -- random number generation
  Author       -- James Fagan (jfagan)
  Created      -- 27-07-2016
  Last updated -- Time-stamp: <2016-08-19 11:54:48 jfagan>
*/

#ifndef __MERSENNE_H__
#define __MERSENNE_H__

#include <random>

inline std::mt19937 & mt() {
  static std::mt19937 e{9876789};
  return e;
}

#endif
