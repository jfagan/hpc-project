/*
  partition.h -- graph partitioning function
  Author       -- James Fagan (jfagan)
  Created      -- 09-07-2016
  Last updated -- Time-stamp: <2016-08-17 12:25:01 jfagan>
*/

#ifndef __PARTITION_H__
#define __PARTITION_H__

namespace Serial {
  
  template<typename site_type>
  class Graph;

}

class Site;

void partition(Serial::Graph<Site> &g, int num_partitions, bool verbose, bool rebalance);

#endif
