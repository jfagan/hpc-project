/*
  infrastructure for serial Potts model on a graph
*/

#ifndef __POTTSGRAPH_H__
#define __POTTSGRAPH_H__

#include "graph.h"
#include <string>

namespace Serial {
  namespace PottsModel {
    template<typename Algorithm>
    class Potts : private Algorithm {

      typedef typename Algorithm::site_type site_type;
      using Algorithm::initialise;
      using Algorithm::perform_sweep;
      using Algorithm::measure_susceptibility;

      Graph<site_type> host;

    public:
      Potts(const Graph<site_type> &g, int q);
    
      // file constructor
      Potts(::std::string &filestub, int q);

      inline const int size  ()         const { return host.size(); }

      inline void reset() { initialise(host); }
      inline void update(const double &beta) { perform_sweep(host, beta); }
    };

#include "tpp/potts.tpp"
  }
}

#endif
