
template <typename Algorithm>
Ising<Algorithm>::Ising(const Graph<site_type> &g) : host{g} {
  initialise(host);
}

template <typename Algorithm>
Ising<Algorithm>::Ising(std::string &filestub) : host{filestub} {
  initialise(host);
}

// template <typename Algorithm>
// Ising<Algorithm>::Ising(int graph_type) {

//   switch(graph_type) {
//   case 1:
//     host = 
