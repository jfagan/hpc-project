

// graph with no edges, only coloured sites
template<typename site_type>
Graph<site_type>::Graph(const int colours, const int size) : total_colours{ colours } {

  sites.resize(size);

  // assign colours to the nodes
  int i=0;
  for(auto &site : sites) {
    site.colour() = (i++)%colours;
  }
}

template<typename site_type>
Graph<site_type>::Graph(const Graph<site_type> &g) {
  total_colours = g.colours();
  sites.resize(g.size());
  for(int i=0; i<g.size(); i++) {
    sites[i] = g[i];      
  }
}

template<typename site_type>
Graph<site_type>::Graph(const ::std::string &filestub) {

  std::size_t found = filestub.find("adj_files");
  std::string filename = filestub;

  if (found==std::string::npos)
    filename = "../adj_files/" + filestub;

  filename += ".adj";

  ::std::ifstream in;
  in.open(filename.c_str(), ::std::ios::in);

  if(!in.is_open())
    throw ::std::ios_base::failure("Failed to open graph file.");

  ::std::string line, item; 

  getline(in, line);
  int size;
  ::std::stringstream(line) >> size;

  this->resize(size);

  getline(in, line);	
  ::std::stringstream(line) >> total_colours;
    
  for(int i=0; i<this->size(); i++) { 
     
    getline(in,line);
    ::std::stringstream ss(line);

    // first number in line is colour	
    ::std::getline(ss,item,',');
    ::std::stringstream(item) >> (*this)[i].colour();

    while(::std::getline(ss,item,',')) {
      int neigh_label;
      ::std::stringstream(item) >> neigh_label ;
      (*this)[i].add_neigh(neigh_label);
    }
  }

  in.close();
}    

template<typename site_type>
void Graph<site_type>::write_adj(const ::std::string &filestub) {

 std::size_t found = filestub.find("adj_files");
 std::string filename = filestub;
 
  if (found==std::string::npos)
    filename = "../adj_files/" + filestub;

  filename += ".adj";

  std::cout<<filename<<std::endl;

   ::std::ofstream out;
  out.open(filename, ::std::ios::out);

  if(!out.is_open())
    throw ::std::ios_base::failure("Failed to open file.");
  
  out << this->size()  <<"\n";
  out << this->colours() <<"\n";

  for(int i=0; i<this->size(); i++) {
    out<<(*this)[i].colour() <<",";
      
    for(int j : (*this)[i].neighbours() ) {
      out<<j<<",";
    }
    out<<"\n";
  }
}

template<typename site_type>
void Graph<site_type>::write_gdf(const ::std::string &filestub)
{
  std::size_t found = filestub.find("gdf_files");
  std::string filename = filestub;
  
  if (found==std::string::npos)
    filename = "../gdf_files/" + filestub;
  
  filename += ".gdf";

  ::std::ofstream out;
  out.open(filename, ::std::ios::out);

  if(!out.is_open())
    throw ::std::ios_base::failure("Failed to open file.");
    
  out<<"nodedef>name VARCHAR, label VARCHAR, colour INTEGER, partition INTEGER\n";

  for(int i=0; i<this->size(); i++) {
    out<<"s"<<i<<", "<<i<<", "<<(*this)[i].colour()<<", "<<(*this)[i].partition()<<::std::endl;
  }

  out<<"edgedef>node1 VARCHAR, node2 VARCHAR\n";
      
  for(int i=0; i<this->size(); i++) {
    for(auto j : (*this)[i].neighbours()) {
      out<<"s"<<i<<", "<<"s"<<j<<"\n";	
    }
  }
  out.close();
}


template<typename site_type>
void Graph<site_type>::write_partition(const ::std::string &filestub, int partitions)
{
  std::size_t found = filestub.find("partition_files");
  std::string filename = filestub;
  
  if (found==std::string::npos)
    filename = "../partition_files/" + filestub;

  filename += ".partition" + std::to_string(partitions);

  ::std::ofstream out;
  out.open(filename, ::std::ios::out);

  if(!out.is_open())
    throw ::std::ios_base::failure("Failed to open file.");
    
  out<<this->size()<<"\n";

  for(int i=0; i<this->size(); i++) {
    out<<(*this)[i].partition()<<::std::endl;
  }
      
  out.close();
}

template<typename site_type>
const Graph<site_type> & Graph<site_type>::operator=(const Graph<site_type> &g) {
  this->total_colours = g.colours();
  this->sites         = g.sites;

  return *this;
}
