
template<typename site_type>
Grid2D<site_type>::Grid2D(int x, int y) : Graph<site_type>{2}, grid_rows{x}, grid_cols{y} {
  
  if (x==2 || y==2) {
    throw std::invalid_argument("Cannot make a grid with two rows or columns, since this causes double links.");
  } else if ( x%2 || y%2 ) {
    throw std::invalid_argument("Must have even number of rows and columns for a two-colourable grid.");
  }
  
  this->sites.resize(x*y);
  
  for(int row=0; row<x; row++) { 
    for(int col=0; col<y; col++ ) {

      (*this)[y*row + col].colour() = (row+col)%2;
	
      this->add_link(y*row + col, (y*(row+1) +  col  )%(x*y) );  // below 
      this->add_link(y*row + col,  y*row     + (col+1)%y     );  // right
      
      // we only need to add_link to the right and below, since the function
      // also adds a link from to target site to us
    }
  }
									     }

static int find_unvisited(std::vector<bool> &in_graph, std::vector<int> visit_order) {
  for( auto i : visit_order ) {
    if (!in_graph[i])
      return i;
  }
  return -1; // no unvisited sites
}

template<typename site_type>
TreeGraph<site_type>::TreeGraph(int x, int y) : Graph<site_type>{2} {

  std::uniform_int_distribution<int> random_walk{0,3};
  
  Grid2D<site_type> grid(x, y); // we generate a tree by getting the uniform spanning tree of a grid
  (*this).resize(x*y);

  // to keep track of which sites have been added to the graph
  std::vector<bool> in_graph( grid.size(), false );
  
  // to decide a random order in which to visit the nodes
  std::vector<int> visit_order( grid.size() );
  std::iota(visit_order.begin(), visit_order.end(), 0); // fill with range
  std::shuffle( visit_order.begin(), visit_order.end(), mt() ); // randomise order

  // WILSONS ALGORITHM
  // 1) choose a random site, x, to be "in the tree"
  // 2) choose another site, y
  // 3) loop erased random walk from y to any site that is already in the tree. Add the path to the tree
  // 4) Repeat 2 and 3 until all sites are in the tree

  // add a random site to the tree, as a seed
  in_graph[ find_unvisited(in_graph, visit_order) ] = true;

  int new_site;
  while( new_site = find_unvisited(in_graph, visit_order), new_site != -1 ) {

    // keep track of the sites in the current search loop
    std::vector<bool> in_loop( grid.size(), false );
    
    // use a list object for our random walk, initialise with site not yet in tree
    std::list<int> path;
    path.push_front(new_site);
    in_loop[path.front()] = true;

    while( !in_graph[ path.front() ]) { 
      // random walk until we find the target
      int choice = random_walk(mt());
      path.push_front( grid[path.front()].neighbour(choice) );

      if( in_loop[path.front()] ) { // implement loop erasing
	int loop_start = path.front();

	do {
	  in_loop[path.front()] = false;
	  path.pop_front();
	} while (path.front() != loop_start);
      }

      in_loop[path.front()] = true;
    }

    // add the loop-erased random walk to the graph
    int previous_element=0;

    for(auto it = path.begin(); it!=path.end(); ++it) {
      in_graph[*it] = true;

      if( *it == path.front() ) {
	previous_element = *it;
	continue;
      }

      (*this).add_link(previous_element, *it);
      previous_element = *it;
    }
  }

  // all trees are two-colourable
  colour_bipartite((*this));
}

template<typename site_type>
RandomGraph<site_type>::RandomGraph(int colours, int size) : Graph<site_type>{colours, size} {
  std::uniform_real_distribution<double> rand_double{0.0, 1.0};
  
  double p = 2*log(size)/size; // almost surely connected (not in this case necessarily)

  bool first_loop = true;

  while(!is_connected(*this)) {

    if(!first_loop) {
      (*this).clear_edges();
    } else {
      first_loop = false;
    }
    
    for(int i=0; i<size; i++) {
      for(int j=0; j<i; j++) {
	if(rand_double(mt()) < p && (*this)[i].colour() != (*this)[j].colour()) {
	  (*this).add_link(i,j);
	}
      }
    }  
  }
}

template<typename site_type>
RandomLocalGraph<site_type>::RandomLocalGraph (int colours, int size) : Graph<site_type>{colours, size} {   
  std::uniform_int_distribution<int> rand_site{0, size-1};
  std::uniform_real_distribution<double> rand_double{0.0, 1.0};

  std::vector<bool> in_graph(size, false);
  in_graph[0] = true;

  for(int i=1; i<size; i++) {

    // pick one random site and connect to it
    int new_connection;
    do {
      new_connection = rand_site(mt());
    } while(!in_graph[new_connection] || (*this)[new_connection].colour() == (*this)[i].colour());

    (*this).add_link(new_connection, i);
    in_graph[i] = true;
    
    // add other links with probability based on current distance from the node
    std::vector<bool> visited(size, false);
    std::vector<int> distances(size, 1<<30);
    visited[i] = true;
    visited[new_connection]=true;
    std::deque<int> queue;

    for(auto j : (*this)[new_connection].neighbours()) {
      if(!visited[j]) {
	queue.push_back(j);
	distances[j] = 2;
      }
    }
    
    while(!queue.empty()) {
      if( visited[queue.front()] ) {
	queue.pop_front();
	continue;
      }
      
      visited[queue.front()] = true;
      
      // attach with probability based on level, degree and graph size
      double p = 10.0/(size*distances[queue.front()]*(*this)[queue.front()].degree());
      
      if(rand_double(mt()) < p && (*this)[queue.front()].colour() != (*this)[i].colour()) {
	(*this).add_link(queue.front(),i);
      }
      
      for(auto j : (*this)[queue.front()].neighbours()) {
	if(!visited[j]) {
	  queue.push_back(j);
	  distances[j] = distances[queue.front()]+1;
	}
      }
      queue.pop_front();
    }
  }
}

