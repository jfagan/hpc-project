
static inline int modulo(int a, int b) { // returns true modulo, i.e a%b is always positive
  return ((a+b) % b);
}

template<typename T>
Serial::Lattice::Ising<T>::Ising(int n) : side_length{n}, rand_double{0.0, 1.0} {

  int totalSpins=n*n;
  spins.resize(totalSpins);
  
  for(int i=0; i<totalSpins; i++) {
    spins[i] = 1;
    if(drand48() < 0.5)
      spins[i] = -1;
  }
}


template<typename T>
int neighbours_agree(const Serial::Lattice::Ising<T> &lattice, int site) {

  int n = lattice.size();
  int L = lattice.side();
  int this_site = lattice[site];
  int agree = 0;

  agree += (this_site == lattice[modulo(site-1, n)]);
  agree += (this_site == lattice[modulo(site+1, n)]);
  agree += (this_site == lattice[modulo(site-L, n)]);
  agree += (this_site == lattice[modulo(site+L, n)]);

  return agree;
} 


template <typename T>
std::ostream& operator<< (std::ostream& strm, const Serial::Lattice::Ising<T> &lattice) {

  strm<<"\n";
  for( int i=0; i<lattice.size(); i++ ) {
    for( int j=0; j<lattice.size(); j++) {
      if (lattice(i,j) == -1)
      	strm << "0 ";
      else
      	strm << "* ";
    }
    strm<<"\n";
  }
  return strm;
}


template<typename T>
void Serial::Lattice::Ising<T>::update(const double &beta){
  
  std::vector<double> flip_probs(5);
  int n = this->size();

  // lookup table of flip probabilities
  flip_probs[0] = exp(-8*beta); // all neighbours disagree
  flip_probs[1] = exp(-4*beta); 
  flip_probs[2] = exp(0);
  flip_probs[3] = exp(4*beta);
  flip_probs[4] = exp(8*beta); // all neighbours agree

  for(int i=0; i<n; i++) {

    double acceptance = flip_probs[neighbours_agree((*this),n)];
    
    if( acceptance > 1 || acceptance > rand_double(mt()) ) {
      (*this)[n] *= -1; 
    }
    
  }
}

template<typename T>
double magnetisation(const Serial::Lattice::Ising<T> &lattice) {
  int mag = 0;
  int N = lattice.size();

  for(int i=0; i<N; i++) {
    for(int j=0; j<N; j++) {
      mag += lattice(i,j);
    }
  } 
  
  return (double)mag/(N*N);
}
