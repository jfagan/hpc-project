

#ifndef __WORM_H__
#define __WORM_H__

#include <vector>
#include <exception>
#include <stdexcept>
#include <algorithm>
#include <iostream>
#include "site.h"
#include "graph.h"

using std::cout;
using std::endl;

class WormSite : public Site {
  std::vector<bool> edges_;
  
public:
  WormSite() : Site::Site(), edges_(neigh.size(), -1) {}
  
  WormSite(const std::vector<int> &n) : Site::Site(n){ 
    edges_.resize(n.size(), false);
  }

  inline void operator=(const WormSite &n) {
    Site::operator=(n);
    edges_ = n.edges();
  }

  // site properties
  inline const std::vector<bool> &edges()                const { return edges_; }
  inline const bool               edge (int neigh_label) const { return edges_[neighbour_index(neigh_label)]; }

  // add a neighbour
  inline void add_neigh(int site_label) {
    if(!this->is_neighbour( site_label ) ) {
      this->neigh.push_back( site_label );
      edges_.push_back(false);
    }
  }

  // remove a neighbour
  inline void del_neigh(int site_label) {
    Site::iterator position = std::find( neigh.begin(), neigh.end(), site_label);
    if( position == neigh.end() ) {
      throw std::out_of_range("Trying to remove non-existent neighbour!");
    } else {
      neigh.erase ( position ); 
      edges_.pop_back();
    }
  }

  inline void clear() { 
    this->neigh.clear(); 
    edges_.clear();
  }

  // return the index of a neighbour
  inline int neighbour_index(int neigh_label) const {
    int i=0;
    while(neigh[i] != neigh_label) { i++; }
    return i;
  }

  // flip link connecting to neighbour
  inline void flip_link(int neigh_label) {
    int index = neighbour_index(neigh_label);
    edges_[index] = !edges_[index];
  }
};

namespace Serial {

  template<typename site_type>
  class Graph;

  namespace IsingModel {

    class Worm {
      int head, tail;
      int coincide;
      std::uniform_real_distribution<double> rand_double;

      void move_head(::Serial::Graph<WormSite> &g, const double &exp_mu);
      void move_tail(::Serial::Graph<WormSite> &g, const double &exp_mu);
      void kick(::Serial::Graph<WormSite> &g);
      
    protected:

      Worm() : rand_double{0.0, 1.0} {}

      typedef WormSite site_type;

      void   initialise             (::Serial::Graph<site_type> &g);
      void   perform_sweep          (::Serial::Graph<site_type> &g, const double &beta);
      double measure_susceptibility (::Serial::Graph<site_type> &g, const double &beta);
    };
  }
}


#endif
