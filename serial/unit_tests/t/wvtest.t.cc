/*
  wvtest.t.cc -- Implementations of tests for wvtest (serial)
  Author       -- James Fagan (jfagan)
  Created      -- 21-06-2016
  Last updated -- Time-stamp: <2016-09-03 18:07:17 jfagan>
*/

#include "wvtest.h"

#include "graph.h"
#include "graph_specialisations.h"
#include "ising.h"
#include "isinglattice.h"
#include "metropolis.h"
#include "mersenne.h"
#include "site.h"

#include <list>
#include <vector>
#include <string>

WVTEST_MAIN("Constructing lattice") {

  // using these flags is more convenient than testing individually
  bool square_degree_flag = 1;
  bool rect_degree_flag = 1;
  bool square_colour_flag = 1;
  bool rect_colour_flag = 1;

  Serial::Grid2D<Site> g(12,12); // square case
  Serial::Grid2D<Site> h(12,8); // rectangular case

  // check sizes
  WVPASSEQ(g.size(), 144);
  WVPASSEQ(h.size(), 96);

  // check colours
  WVPASSEQ(g.colours(), 2);
  WVPASSEQ(h.colours(), 2);

  // check connectivity
  WVPASS(is_connected(g));
  WVPASS(is_connected(h));

  // check all sites have the correct degree
  for(int i=0; i<g.size(); i++) {
    square_degree_flag &= (g[i].degree() == 4);
  }

  WVPASS(square_degree_flag);

  for(int i=0; i<h.size(); i++) {
    rect_degree_flag &= (h[i].degree()==4);
  }

  WVPASS(rect_degree_flag);

  // no site should have the same colour as one of its neighbours
  for(int i=0; i<g.size(); i++) {
    for( int j : g[i].neighbours() ) {
      square_colour_flag &= (g[i].colour() != g[j].colour() );
    }
  }

  WVPASS(square_colour_flag);

  for(int i=0; i<h.size(); i++) {
    for( int j : h[i].neighbours() ) {
      rect_colour_flag &= ( h[i].colour() != h[j].colour() );
    }
  } 

  WVPASS(rect_colour_flag);
}

WVTEST_MAIN("Generating uniform spanning tree of lattice") {

  Serial::TreeGraph<Site> g(10,10);

  WVPASSEQ(g.colours(), 2);
  WVPASSEQ(g.size(), 100);
  WVPASS(is_connected(g));

  int total_edges=0;

  for(int i=0; i<g.size(); i++) {
    total_edges += g[i].degree();
  }

  // allow for double counting
  total_edges/=2;

  WVPASSEQ(total_edges, g.size()-1); // must be true for a tree
  
  // use a depth first search to check that there are no loops
  std::list<int> stack;
  std::vector<bool> visited(g.size(), false);
  
  g[0].colour() = 0;
  stack.push_back(0);
  visited[0] = true;
  int last_site=0;
  bool tree_flag = true;

  while( !stack.empty() ) {
    
    // check that no prospective sites have been visited before (back edge => loop exists)
    for(auto i : g[stack.front()].neighbours()) {  
      if(visited[i] && i != last_site ) {
	tree_flag = false;
      } 
    }

    // find the next site to visit
    last_site = stack.front();
    for(auto i : g[stack.front()].neighbours()) {
      if(!visited[i] && i != last_site) {
	stack.push_back(i);
	break;
      }
    }

    // if there are no available sites to move to, backtrace
    if( stack.front() == last_site ) {
      visited[ stack.front() ] = true;
      stack.pop_front();
    }
  }

  WVPASS(tree_flag);
}

WVTEST_MAIN("Generating random graph") {

  Serial::RandomGraph<Site> g(4, 100);
  bool colour_flag=true;

  WVPASSEQ(g.colours(), 4);
  WVPASSEQ(g.size(), 100);
  WVPASS(is_connected(g)); // this is enforced at construction, should always pass

  // no site should have the same colour as one of its neighbours
  for(int i=0; i<g.size(); i++) {
    for( int j : g[i].neighbours() ) {
      colour_flag &= (g[i].colour() != g[j].colour() );
    }
  }

  WVPASS(colour_flag);
}


WVTEST_MAIN("Generating local random graph") {

  Serial::RandomLocalGraph<Site> g(4, 100);
  bool colour_flag=true;

  WVPASSEQ(g.colours(), 4);
  WVPASSEQ(g.size(), 100);
  WVPASS(is_connected(g)); // this is enforced at construction, should always pass

  // no site should have the same colour as one of its neighbours
  for(int i=0; i<g.size(); i++) {
    for( int j : g[i].neighbours() ) {
      colour_flag &= (g[i].colour() != g[j].colour() );
    }
  }

  WVPASS(colour_flag);
}

WVTEST_MAIN("Adding and removing links between sites") {

  Serial::Grid2D<Site> g(10,10);

  // should be connected in the first place
  WVPASS(g[1].is_neighbour(2));
  WVPASS(g[2].is_neighbour(1));

  g.remove_link(1,2);

  // check that neither site thinks they're still connected
  WVFAIL(g[1].is_neighbour(2));
  WVFAIL(g[2].is_neighbour(1));

  g.add_link(1,2);

  // now they should both know that they're connected again
  WVPASS(g[1].is_neighbour(2));
  WVPASS(g[2].is_neighbour(1));
}

WVTEST_MAIN("File I/O") {

  Serial::Grid2D<Site> g(10,10);

  g.write_adj("../../adj_files/gridtest");
  
  Serial::Graph<Site> h("../../adj_files/gridtest");

  WVPASS(g.size() == h.size());
  WVPASS(g.colours() == h.colours());

  bool degree_flag=1;
  bool neighbour_flag=1;

  for(int i=0; i<g.size(); i++) {
    degree_flag &= (g[i].degree() == h[i].degree());

    for(int j=0; j<g[i].degree(); j++) {
      neighbour_flag &= (g[i].neighbour(j) == h[i].neighbour(j));
    }
  }

  WVPASS(degree_flag);
  WVPASS(neighbour_flag);
}

WVTEST_MAIN("Graph copy constructor") {

  Serial::Grid2D<Site> g(10,10);
  Serial::Graph<Site> h(g);

  WVPASS(g.size() == h.size());
  WVPASS(g.colours() == h.colours());

  bool degree_flag=1;
  bool neighbour_flag=1;

  for(int i=0; i<g.size(); i++) {
    degree_flag &= (g[i].degree() == h[i].degree());

    for(int j=0; j<g[i].degree(); j++) {
      neighbour_flag &= (g[i].neighbour(j) == h[i].neighbour(j));
    }
  }

  WVPASS(degree_flag);
  WVPASS(neighbour_flag);

}


WVTEST_MAIN("Graph Ising configuration occurence rates") {

 Serial::Graph<MetropolisSite<char> > g(2,3); // two colours, three sites

  g.add_link(0,1);
  g.add_link(1,2);

  Serial::IsingModel::Ising<Serial::IsingModel::Metropolis> ising(g);
  double beta = 0.1;

  std::vector<double> occurence(8, 0.0);          // 4 distinct configurations
  std::vector<double> expected_occurence(8, 0.0);

  // energy is -1 for an aligned bond, +1 for an anti-aligned bond
  // order the configs by a binary rep, -1 is 0, 1 is 1

  expected_occurence[0] = exp(2*beta); // down down down
  expected_occurence[1] = exp(0);       // down down up
  expected_occurence[2] = exp(-2*beta);  // down up   down
  expected_occurence[3] = exp(0);       // down up   up
  expected_occurence[4] = exp(0);       // up   down down
  expected_occurence[5] = exp(-2*beta);  // up   down up
  expected_occurence[6] = exp(0);       // up   up   down
  expected_occurence[7] = exp(2*beta); // up   up   up

  double Z = 0; // partition function
  for(auto i : expected_occurence)
    Z += i;

  // normalise the probabilities
  for(auto &i : expected_occurence)
    i/=Z;

  int iter = 1000000;
  double tol = 1e-2; // the tolerance of our equality test on the probabilities

  for(int i=0; i<0.1*iter; i++) {
    ising.update(beta);
  }

  for(int i=0; i<iter; i++) {
    ising.update(beta);
    
    int index = 0;

    // bit shifting  is more efficient than using if statements
    // note that the configurations are ordered by a binary rep, -1 is 0, 1 is 1
    index += ( ising.graph()[0].value() == 1) << 2;
    index += ( ising.graph()[1].value() == 1) << 1;
    index += ( ising.graph()[2].value() == 1) << 0;
  
    occurence[index]++;
  }
  
  for (auto &i: occurence)
    i/=iter;

  bool flag = true;
  
  for(int i=0; i<2; i++) {
    flag &= (std::abs(occurence[i]-expected_occurence[i]) < tol*expected_occurence[i] );
  }

  WVPASS(flag);
}
