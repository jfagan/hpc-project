all: serial parallel

serial: 
	make -C serial/

parallel:
	make -C parallel/

serialtest:
	make -C serial/ test

paralleltest:
	make -C parallel/ test

test: serialtest paralleltest

.PHONY: clean serial parallel serialtest paralleltest

clean:
	make -C serial/ clean
	make -C parallel/ clean
