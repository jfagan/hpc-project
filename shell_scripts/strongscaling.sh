#!/bin/bash

cd data/strong_scaling

for graph in grid tree
do
    for size in 10000 15000 20000 25000 30000
    do
	serialtime=$(cat "${graph}${size}_serial.dat")
	for file in ${graph}${size}${col}_bitwise*
	do
	    echo $file
	    while read p t1 s1 t2 s2
	    do 
		part=$(cat "../partition/${graph}${size}-${p}_partitions.dat")
		echo -e "${p}\t${t1}\t${s1}\t$(bc <<< "scale=4; 100*(${s1}/${p})")\t${t2}\t${s2}\t$(bc <<< "scale=4; 100*(${s2}/${p})")" >> temp.dat
		  #  echo -e "${p}\t${t}\t$(bc <<< "scale=4; ${serialtime}/${t}")" >> temp.dat
	    done < $file
	    mv temp.dat $file
	done
    done
done


for graph in rand randlocal
do
    for col in _col2 _col3 _col4
    do
	for size in 10000 15000 20000 25000 30000
	do
	    serialtime=$(cat "${graph}${size}${col}_serial.dat")
	    for file in ${graph}${size}${col}_bitwise*
	    do
		echo $file
		while read p t1 s1 t2 s2
		do 
		    part=$(cat "../partition/${graph}${size}${col}-${p}_partitions.dat")
		    echo -e "${p}\t${t1}\t${s1}\t$(bc <<< "scale=4; 100*(${s1}/${p})")\t${t2}\t${s2}\t$(bc <<< "scale=4; 100*(${s2}/${p})")" >> temp.dat
		 #   echo -e "${p}\t${t}\t${s}\t$(bc <<< "scale=4; ${part} + ${t}")\t$(bc <<< "scale=4; ${serialtime}/(${part} + ${t})")" >> temp.dat
		  #  echo -e "${p}\t${t}\t$(bc <<< "scale=4; ${serialtime}/${t}")" >> temp.dat
		done < $file
		mv temp.dat $file
	    done
	done
    done
done

cd -