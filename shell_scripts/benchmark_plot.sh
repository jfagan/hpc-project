#!/bin/bash


for file in "tree10000" "grid10000" "rand10000"
do
    for sub in "_balanced" "_unbalanced"
    do
	gnuplot -e "file='${file}${sub}.dat'" -e "target='figs/benchmark/${file}${sub}.png'" shell_scripts/benchmark.plot
    done
done
