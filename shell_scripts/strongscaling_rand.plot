set terminal pngcairo enhanced
print figname
print file
 
set grid lw 2
set key left

set xlabel "Processes"
set ylabel "Speedup"

set ytics 1

set log x 2

set style line 5 lc rgb '#8b1a0e'  pt 1 ps 1 lt 1 lw 2 # --- red
set style line 4 lc rgb '#5e9c36'  pt 2 ps 1 lt 1 lw 2 # --- green
set style line 3 lc rgb '0x00008B' pt 3 ps 1 lt 1 lw 2 # --- blue
set style line 2 lc rgb '0xFF8C00'  pt 4 ps 1 lt 1 lw 2 # --- green
set style line 1 lc rgb '0x9932CC'  pt 5 ps 1 lt 1 lw 2 # --- green

set style line 11 lc rgb '#808080' lt 1
set border 3 back ls 11
set tics nomirror

set out figname."_bitwise.png"

plot file."10000".colours."_bitwise.dat" u 1:3 w lp ls 1 t "10,000 sites",\
     file."15000".colours."_bitwise.dat" u 1:3 w lp ls 2 t "15,000 sites",\
     file."20000".colours."_bitwise.dat" u 1:3 w lp ls 3 t "20,000 sites",\
     file."25000".colours."_bitwise.dat" u 1:3 w lp ls 4 t "25,000 sites",\
     file."30000".colours."_bitwise.dat" u 1:3 w lp ls 5 t "30,000 sites"
     

set out figname."_bitwisepersistent.png"

plot file."10000".colours."_bitwisepersistent.dat" u 1:3 w lp ls 1 t "10,000 sites",\
     file."15000".colours."_bitwisepersistent.dat" u 1:3 w lp ls 2 t "15,000 sites",\
     file."20000".colours."_bitwisepersistent.dat" u 1:3 w lp ls 3 t "20,000 sites",\
     file."25000".colours."_bitwisepersistent.dat" u 1:3 w lp ls 4 t "25,000 sites",\
     file."30000".colours."_bitwisepersistent.dat" u 1:3 w lp ls 5 t "30,000 sites"
     
set out figname."_bitwise_withpartition.png"

plot file."10000".colours."_bitwise.dat" u 1:6 w lp ls 1 t "10,000 sites",\
     file."15000".colours."_bitwise.dat" u 1:6 w lp ls 2 t "15,000 sites",\
     file."20000".colours."_bitwise.dat" u 1:6 w lp ls 3 t "20,000 sites",\
     file."25000".colours."_bitwise.dat" u 1:6 w lp ls 4 t "25,000 sites",\
     file."30000".colours."_bitwise.dat" u 1:6 w lp ls 5 t "30,000 sites"
     

set out figname."_bitwisepersistent_withpartition.png"

plot file."10000".colours."_bitwisepersistent.dat" u 1:6 w lp ls 1 t "10,000 sites",\
     file."15000".colours."_bitwisepersistent.dat" u 1:6 w lp ls 2 t "15,000 sites",\
     file."20000".colours."_bitwisepersistent.dat" u 1:6 w lp ls 3 t "20,000 sites",\
     file."25000".colours."_bitwisepersistent.dat" u 1:6 w lp ls 4 t "25,000 sites",\
     file."30000".colours."_bitwisepersistent.dat" u 1:6 w lp ls 5 t "30,000 sites"
     