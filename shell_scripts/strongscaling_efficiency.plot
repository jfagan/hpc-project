set terminal pngcairo enhanced
print figname
print file
 
set grid lw 2
set key right

set yrange[0:120]

set xlabel "Processes"
set ylabel "Efficiency (%)"

set log x 2

set style line 5 lc rgb '#8b1a0e'  pt 1 ps 1 lt 1 lw 2 # --- red
set style line 4 lc rgb '#5e9c36'  pt 2 ps 1 lt 1 lw 2 # --- green
set style line 3 lc rgb '0x00008B' pt 3 ps 1 lt 1 lw 2 # --- pink
set style line 2 lc rgb '0xFF8C00'  pt 4 ps 1 lt 1 lw 2 # --- green
set style line 1 lc rgb '0x9932CC'  pt 5 ps 1 lt 1 lw 2 # --- green

set style line 11 lc rgb '#808080' lt 1
set border 3 back ls 11
set tics nomirror

set out figname."_efficiency_bitwise.png"

plot file."10000_bitwise.dat" u 1:4 w lp ls 1 t "10,000 sites",\
     file."15000_bitwise.dat" u 1:4 w lp ls 2 t "15,000 sites",\
     file."20000_bitwise.dat" u 1:4 w lp ls 3 t "20,000 sites",\
     file."25000_bitwise.dat" u 1:4 w lp ls 4 t "25,000 sites",\
     file."30000_bitwise.dat" u 1:4 w lp ls 5 t "30,000 sites"
     

set out figname."efficiency_bitwisepersistent.png"

plot file."10000_bitwisepersistent.dat" u 1:4 w lp ls 1 t "10,000 sites",\
     file."15000_bitwisepersistent.dat" u 1:4 w lp ls 2 t "15,000 sites",\
     file."20000_bitwisepersistent.dat" u 1:4 w lp ls 3 t "20,000 sites",\
     file."25000_bitwisepersistent.dat" u 1:4 w lp ls 4 t "25,000 sites",\
     file."30000_bitwisepersistent.dat" u 1:4 w lp ls 5 t "30,000 sites"

set out figname."efficiency_bitwise_withpartition.png"

plot file."10000_bitwise.dat" u 1:7 w lp ls 1 t "10,000 sites",\
     file."15000_bitwise.dat" u 1:7 w lp ls 2 t "15,000 sites",\
     file."20000_bitwise.dat" u 1:7 w lp ls 3 t "20,000 sites",\
     file."25000_bitwise.dat" u 1:7 w lp ls 4 t "25,000 sites",\
     file."30000_bitwise.dat" u 1:7 w lp ls 5 t "30,000 sites"
     

set out figname."efficiency_bitwisepersistent_withpartition.png"

plot file."10000_bitwisepersistent.dat" u 1:7 w lp ls 1 t "10,000 sites",\
     file."15000_bitwisepersistent.dat" u 1:7 w lp ls 2 t "15,000 sites",\
     file."20000_bitwisepersistent.dat" u 1:7 w lp ls 3 t "20,000 sites",\
     file."25000_bitwisepersistent.dat" u 1:7 w lp ls 4 t "25,000 sites",\
     file."30000_bitwisepersistent.dat" u 1:7 w lp ls 5 t "30,000 sites"