#!/bin/bash

for file in grid tree
do 
    gnuplot -e "figname='figs/susceptibility/${file}.png'" -e "file='data/susceptibility/${file}'" shell_scripts/susceptibility.plot
done


for file in rand randlocal
do 
    for col in _col2 _col3 _col4
    do
	gnuplot -e "figname='figs/susceptibility/${file}${col}.png'" -e "file='data/susceptibility/${file}'"  shell_scripts/susceptibility_rand.plot
    done
done
