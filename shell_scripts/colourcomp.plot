set terminal pngcairo enhanced
 
set grid
set key left

set xlabel "Processes"
set ylabel "Speedup"

set ytics 1

set log x 2

set style line 5 lc rgb '#8b1a0e'  pt 1 ps 1 lt 1 lw 2 # --- red
set style line 4 lc rgb '#5e9c36'  pt 2 ps 1 lt 1 lw 2 # --- green
set style line 3 lc rgb '0x00008B' pt 3 ps 1 lt 1 lw 2 # --- pink
set style line 2 lc rgb '0xFF8C00'  pt 4 ps 1 lt 1 lw 2 # --- green
set style line 1 lc rgb '0x9932CC'  pt 5 ps 1 lt 1 lw 2 # --- green

set style line 11 lc rgb '#808080' lt 1
set border 3 back ls 11
set tics nomirror

set out "figs/colourcomp.png"

plot "data/colours/grid14400_col2.dat" u 1:3 w lp ls 1 t "Two colours",\
     "data/colours/grid14400_col3.dat" u 1:3 w lp ls 2 t "Three colours",\
     "data/colours/grid14400_col4.dat" u 1:3 w lp ls 3 t "Four colours",\
     "data/colours/grid14400_col5.dat" u 1:3 w lp ls 4 t "Five colours",\
     "data/colours/grid14400_col6.dat" u 1:3 w lp ls 5 t "Six colours"
