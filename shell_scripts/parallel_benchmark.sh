#!/bin/bash

# for file in grid10000 tree10000 rand10000
# do
#     echo $file

#     unbalanced="data/benchmark/${dir}/${file}_unbalanced.dat"
#     balanced="data/benchmark/${dir}/${file}_balanced.dat"

#     rm $unbalanced
#     rm $balanced

#     for p in 1 2 4 8
#     do
# 	echo "unbalanced $p"
# 	cd parallel
# 	mpirun -n $p bin/benchmark -f $file -i 10000 -v >> "../${unbalanced}"
# 	cd - > /dev/null

# 	echo "balanced $p"
# 	cd parallel
# 	mpirun -n $p bin/benchmark -f $file -i 10000 -v -r >> "../${balanced}"
# 	cd - > /dev/null
#     done
# done


# for size in 20000
# do
#     for file in "randlocal${size}_col2" "randlocal${size}_col3" "randlocal${size}_col4"
#     do
# 	target="data/large_benchmark/${file}_nonblocking.dat"
# 	echo "" > $target

# 	for p in 1 2 4 8
# 	do
# 	    echo "${file} nonblocking ${p}"
# 	    cd parallel
# 	    mpirun -n $p bin/benchmark -f $file -i 100000 -v -r -c 0 >> "../${target}"
# 	    cd - > /dev/null
# 	done

# 	target="data/large_benchmark/${file}_persistent.dat"
# 	echo "" > $target

# 	for p in 1 2 4 8
# 	do
# 	    echo "${file} persistent ${p}"
# 	    cd parallel
# 	    mpirun -n $p bin/benchmark -f $file -i 100000 -v -r -c 1 >> "../${target}"
# 	    cd - > /dev/null
# 	done

# 	target="data/large_benchmark/${file}_bitwise.dat"
# 	echo "" > $target

# 	for p in 1 2 4 8
# 	do
# 	    echo "${file} bitwise ${p}"
# 	    cd parallel
# 	    mpirun -n $p bin/benchmark -f $file -i 100000 -v -r -c 2 >> "../${target}"
# 	    cd - > /dev/null
# 	done

# 	target="data/large_benchmark/${file}_bitwisepersistent.dat"
# 	echo "" > $target

# 	for p in 1 2 4 8
# 	do
# 	    echo "${file} persistent bitwise ${p}"
# 	    cd parallel
# 	    mpirun -n $p bin/benchmark -f $file -i 100000 -v -r -c 3 >> "../${target}"
# 	    cd - > /dev/null
# 	done
#     done
# done


# for size in 30000
# do
#     for file in "randlocal${size}_col3" "rand${size}_col4" "randlocal${size}_col4"  
#     do
# 	target="data/large_benchmark/${file}_bitwise.dat"
# 	echo "" > $target

# 	for p in 1 2 4 8
# 	do
# 	    echo "${file} bitwise ${p}"
# 	    cd parallel
# 	    mpirun -n $p bin/benchmark -f $file -i 100000 -v -r -c 2 >> "../${target}"
# 	    cd - > /dev/null
# 	done

# 	target="data/large_benchmark/${file}_bitwisepersistent.dat"
# 	echo "" > $target

# 	for p in 1 2 4 8
# 	do
# 	    echo "${file} persistent bitwise ${p}"
# 	    cd parallel
# 	    mpirun -n $p bin/benchmark -f $file -i 100000 -v -r -c 3 >> "../${target}"
# 	    cd - > /dev/null
# 	done
#     done
# done


for size in 15000
do
    for file in "rand${size}_col3" "randlocal${size}_col3" "rand${size}_col4" "randlocal${size}_col4"
    do
	target="data/large_benchmark/${file}_bitwise.dat"

	for p in 1 2 4 8
	do
	    echo "${file} bitwise ${p}"
	    cd parallel
	    mpirun -n $p bin/benchmark -f $file -i 100000 -v -r -c 2 >> "../${target}"
	    cd - > /dev/null
	done

	target="data/large_benchmark/${file}_bitwisepersistent.dat"

	for p in 1 2 4 8
	do
	    echo "${file} persistent bitwise ${p}"
	    cd parallel
	    mpirun -n $p bin/benchmark -f $file -i 100000 -v -r -c 3 >> "../${target}"
	    cd - > /dev/null
	done
    done
done