set terminal pngcairo enhanced
print figname
print file
 
set grid
set key left

set xlabel "Beta"
set ylabel "< (avg spin)^2 >"

set yrange [0:1]
set xrange [0.05:1]

set style line 3 lc rgb '#8b1a0e'  pt 1 ps 1 lt 1 lw 2 # --- red
set style line 2 lc rgb '#5e9c36'  pt 2 ps 1 lt 1 lw 2 # --- green
set style line 1 lc rgb '0x00008B' pt 3 ps 1 lt 1 lw 2 # --- pink

set style line 11 lc rgb '#808080' lt 1
set border 3 back ls 11
set tics nomirror

set out figname

plot file."100.dat" u 2:3 w lp ls 1 t "100 sites",\
     file."1600.dat" u 2:3 w lp ls 2 t "1,600 sites",\
     file."10000.dat" u 2:3 w lp ls 3 t "10,000 sites",\