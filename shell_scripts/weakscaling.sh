#!/bin/bash

cd data/weak_scaling

for graph in grid tree rand randlocal
do
    for file in ${graph}_bitwise*
    do
	echo $file
	read proc time1 < $file
	while read p t
	do 
	    echo -e "${p}\t${t}\t$(bc <<< "scale=4; (100*${time1})/${t}")" >> temp.dat
		  #  echo -e "${p}\t${t}\t$(bc <<< "scale=4; ${serialtime}/${t}")" >> temp.dat
	done < $file
	mv temp.dat $file
    done
done

cd -