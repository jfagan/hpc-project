#!/bin/bash

cd serial

# for prefix in rand randlocal
# do
#     for suffix in _col2 _col3 _col4
#     do
# 	for p in 1 2 4 8 16 32 64
# 	do
# 	    n=$((500*p))
# 		echo "weak ${prefix}${n}${suffix} $p"
# 		{ /usr/bin/time --format="%e" ./bin/partition -f "${prefix}${n}${suffix}" -r -p $p; } &>> "../data/partition/${prefix}${n}${suffix}-${p}_partitions.dat"
# 	done
#     done
# done

# for prefix in rand randlocal
# do
#     for suffix in _col2 _col3 _col4
#     do
# 	for n in 10000 15000 20000 25000 30000
# 	do
# 	    for p in 1 2 4 8 16 32 64
# 	    do
# 		echo "${prefix}${n}${suffix} $p"
# 		{ /usr/bin/time --format="%e" ./bin/partition -f "${prefix}${n}${suffix}" -r -p $p; } &>> "../data/partition/${prefix}${n}${suffix}-${p}_partitions.dat"
# 	    done
# 	done
#     done
# done


for prefix in tree
do
    for p in 1 2 4 8 16 32 64
    do
	n=$((500*p))
	echo "weak ${prefix}${n} $p"
	{ /usr/bin/time --format="%e" ./bin/partition -f "${prefix}${n}" -r -p $p; } &>> "../data/partition/${prefix}${n}-${p}_partitions.dat"
    done
done


for prefix in tree
do
    for n in 10000 15000 20000 25000 30000
    do
	for p in 1 2 4 8 16 32 64
	do
	    echo "${prefix}${n} $p"
	    { /usr/bin/time --format="%e" ./bin/partition -f "${prefix}${n}" -r -p $p; } &>> "../data/partition/${prefix}${n}-${p}_partitions.dat"
	done
    done
done


# for p in 1 2 4 8 16 32 64
# do
#     echo "grid $p"
#     n=$(( 500*p))

#     echo "------------------------------------------" >> "../data/partition/weak_grid.dat"
#     echo "${prefix}${n}" >> "../data/partition/weak_grid.dat"
#     echo "$p partitions" >> "../data/partition/weak_grid.dat"
#     echo "" >> "../data/partition/weak_grid.dat"
#     { time ./bin/partition -f "grid${n}" -r -p $p -v; } &>> "../data/partition/weak_grid.dat"
#     echo "------------------------------------------" >> "../data/partition/weak_grid.dat"
#     echo "" >> "../data/partition/weak_grid.dat"
	
# done

# for prefix in grid tree
# do
#     #echo "" > "../data/partition/${prefix}.dat"
#     for n in 15000 25000
#     do
# 	echo "${prefix}${n}"
# 	for p in 1 2 4 8 16 32 64
# 	do
# 	    echo "" >> "../data/partition/${prefix}.dat"
# 	    echo "------------------------------------------" >> "../data/partition/${prefix}.dat"
# 	    echo "${prefix}${n}" >> "../data/partition/${prefix}.dat"
# 	    echo "" >> "../data/partition/${prefix}.dat"
# 	    { time ./bin/partition -f "${prefix}${n}" -r -p $p -v; } &>> "../data/partition/${prefix}.dat"
# 	    echo "------------------------------------------" >> "../data/partition/${prefix}.dat"
# 	    echo "" >> "../data/partition/${prefix}.dat"
	   
# 	done
#     done
# done


# for prefix in tree
# do
#     for n in 30000
#     do
# 	echo "${prefix}${n}"
# 	for p in 16 32 64
# 	do
# 	    echo "" >> "../data/partition/${prefix}.dat"
# 	    echo "------------------------------------------" >> "../data/partition/${prefix}.dat"
# 	    echo "${prefix}${n}" >> "../data/partition/${prefix}.dat"
# 	    echo "$p partitions" >> "../data/partition/${prefix}$.dat"
# 	    echo "" >> "../data/partition/${prefix}.dat"
# 	    { time ./bin/partition -f "${prefix}${n}" -r -p $p -v; } &>> "../data/partition/${prefix}.dat"
# 	    echo "------------------------------------------" >> "../data/partition/${prefix}.dat"
# 	    echo "" >> "../data/partition/${prefix}.dat"
	   
# 	done
#     done
# done


# for prefix in tree
# do
#     for n in 40000 50000
#     do
# 	echo "${prefix}${n}"
# 	for p in 1 2 4 8 16 32 64
# 	do
# 	    echo "" >> "../data/partition/${prefix}.dat"
# 	    echo "------------------------------------------" >> "../data/partition/${prefix}.dat"
# 	    echo "${prefix}${n}" >> "../data/partition/${prefix}.dat"
# 	    echo "$p partitions" >> "../data/partition/${prefix}$.dat"
# 	    echo "" >> "../data/partition/${prefix}.dat"
# 	    { time ./bin/partition -f "${prefix}${n}" -r -p $p -v; } &>> "../data/partition/${prefix}.dat"
# 	    echo "------------------------------------------" >> "../data/partition/${prefix}.dat"
# 	    echo "" >> "../data/partition/${prefix}.dat"
	   
# 	done
#     done
# done
    
cd -