set terminal pngcairo enhanced
set out figname
print figname
print metrofile
print clusterfile

set grid

set xlabel "Beta"
set ylabel "Integrated Autocorrelation Time"

set style line 1 lc rgb '#8b1a0e' pt 1 ps 1 lt 1 lw 2 # --- red
set style line 2 lc rgb '#5e9c36' pt 6 ps 1 lt 1 lw 2 # --- green

set style line 11 lc rgb '#808080' lt 1
set border 3 back ls 11
set tics nomirror

plot metrofile u 2:3 w lp t "Metropolis" ls 1, clusterfile u 2:3 w lp t "Wolff Cluster" ls 2