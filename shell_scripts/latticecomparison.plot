set terminal pngcairo enhanced
set out "figs/latticecomparison.png"
file="data/lattice_comparison.dat"

set grid
set key left


set xlabel "Lattice side length"
set ylabel "Time (s)"

set style line 1 lc rgb '#8b1a0e' pt 1 ps 1 lt 1 lw 2 # --- red
set style line 2 lc rgb '#5e9c36' pt 6 ps 1 lt 1 lw 2 # --- green

set style line 11 lc rgb '#808080' lt 1
set border 3 back ls 11
set tics nomirror

f(x) = a*x*x
g(x) = b*x*x

fit f(x) file u 1:2 via a
fit g(x) file u 1:3 via b

plot f(x) ls 1 t sprintf("Lattice: %.2e x^2",a), \
     g(x) ls 2 t sprintf("Graph : %.2e x^2",b), \
     file u 1:2 notitle ps 1, \
     file u 1:3 notitle ps 1
