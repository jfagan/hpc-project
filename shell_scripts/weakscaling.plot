set terminal pngcairo enhanced
 
set grid
set key right

set yrange [0:120]

set xlabel "Processes"
set ylabel "Efficiency (%)"

set log x 2

set style line 4 lc rgb '#8b1a0e'  pt 1 ps 1 lt 1 lw 2 # --- red
set style line 3 lc rgb '#5e9c36'  pt 2 ps 1 lt 1 lw 2 # --- green
set style line 2 lc rgb '0x00008B' pt 3 ps 1 lt 1 lw 2 # --- pink
set style line 1 lc rgb '0xFF8C00'  pt 4 ps 1 lt 1 lw 2 # --- green

set style line 11 lc rgb '#808080' lt 1
set border 3 back ls 11
set tics nomirror

set out fig."bitwise.png"

plot dir."grid_bitwise.dat" u 1:3 w lp ls 1 t "Lattice",\
     dir."tree_bitwise.dat" u 1:3 w lp ls 2 t "Tree",\
     dir."rand_bitwise.dat" u 1:3 w lp ls 3 t "Random",\
     dir."randlocal_bitwise.dat" u 1:3 w lp ls 4 t "Random local",\

set out fig."bitwisepersistent.png"


plot dir."grid_bitwisepersistent.dat" u 1:3 w lp ls 1 t "Lattice",\
     dir."tree_bitwisepersistent.dat" u 1:3 w lp ls 2 t "Tree",\
     dir."rand_bitwisepersistent.dat" u 1:3 w lp ls 3 t "Random",\
     dir."randlocal_bitwisepersistent.dat" u 1:3 w lp ls 4 t "Random local",\