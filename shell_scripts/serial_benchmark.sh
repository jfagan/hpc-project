#!/bin/bash

#SBATCH -N 1        
#SBATCH -p compute
#SBATCH -t 13:30:00  
#SBATCH -U mschpc
#SBATCH -J ising
#SBATCH -o serial.out

# source the module commands
source /etc/profile.d/modules.sh

# load the modules used to build the xhpl binary
module load cports gcc/4.9.3-gnu

cd serial
# for size in 10000 15000 20000 25000 30000
# do 
#     for col in _col2 _col3 _col4
#     do
# 	for pre in rand randlocal 
# 	do
# 	    file="${pre}${size}${col}"
# 	    target="data/strong_scaling/${file}_serial.dat"
	    
# 	    echo $file  
# 	    ./bin/benchmark -f $file -i 100000 -v >> "../${target}"
# 	done
#     done
# done


for size in 10000 15000 20000 25000 30000
do
    for pre in "grid" "tree"
    do
	file="${pre}${size}"
	target="data/strong_scaling/${file}_serial.dat"
	
	echo "${file} nonblocking ${p}"
	./bin/benchmark -f $file -i 100000 -v >> "../${target}"
    done
    
done

cd -