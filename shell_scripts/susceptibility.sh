#!/bin/bash

cd serial
for file in tree #grid
do
    for size in 100 1600 10000
    do
	./bin/isingmeasure -f "${file}${size}" -M -i 100000 -m 100 -s 0.01 -e 1000 > "../data/susceptibility/${file}${size}.dat"
    done
done

# for file in randlocal
# do
#     for size in 100 1600 10000
#     do
# 	./bin/isingmeasure -f "${file}${size}_col2" -M -i 100000 -m 105 -s 1 -e 2 >> "../data/susceptibility/${file}${size}_col2.dat"
#     done
# done

cd -