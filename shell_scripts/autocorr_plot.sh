#!/bin/bash

p="data/autocorrelation_times/"
s="_0.3_0.6_100"

for n in 400 900 1600 2500
do 
    gnuplot -e "figname='figs/autocorr/grid${n}${s}.png'" -e "metrofile='${p}M_grid${n}${s}.dat'" -e "clusterfile='${p}C_grid${n}${s}.dat'" shell_scripts/grid_autocorr.plot
done

s="_0.05_0.3_100"
for c in "_col2" "_col3" "_col4"
do
    for n in 400 900 1600 2500
    do 
	gnuplot -e "figname='figs/autocorr/rand${n}${c}${s}.png'" -e "metrofile='${p}M_rand${n}${c}${s}.dat'" -e "clusterfile='${p}C_rand${n}${c}${s}.dat'" shell_scripts/rand_autocorr.plot
    done
done

s="_0.4_0.9_100"
for c in "_col2" "_col3" "_col4"
do
    for n in 400 900 1600 2500
    do 
	gnuplot -e "figname='figs/autocorr/randlocal${n}${c}${s}.png'" -e "metrofile='${p}M_randlocal${n}${c}${s}.dat'" -e "clusterfile='${p}C_randlocal${n}${c}${s}.dat'" shell_scripts/randlocal_autocorr.plot
    done
done
