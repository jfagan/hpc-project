#!/bin/bash

for file in grid tree
do 
    gnuplot -e "figname='figs/strong_scaling/scaling/${file}'" -e "file='data/strong_scaling/${file}'" shell_scripts/strongscaling.plot
done


for file in rand randlocal
do 
    for col in _col2 _col3 _col4
    do
	gnuplot -e "figname='figs/strong_scaling/scaling/${file}${col}'" -e "file='data/strong_scaling/${file}'" -e "colours='${col}'" shell_scripts/strongscaling_rand.plot
    done
done

for file in grid tree
do 
    gnuplot -e "figname='figs/strong_scaling/efficiency/${file}'" -e "file='data/strong_scaling/${file}'" shell_scripts/strongscaling_efficiency.plot
done


for file in rand randlocal
do 
    for col in _col2 _col3 _col4
    do
	gnuplot -e "figname='figs/strong_scaling/efficiency/${file}${col}'" -e "file='data/strong_scaling/${file}'" -e "colours='${col}'" shell_scripts/strongscaling_rand_efficiency.plot
    done
done
