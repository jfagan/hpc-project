#!/bin/bash

cd serial


for file in randlocal400_col2 randlocal900_col2 randlocal1600_col2 randlocal2500_col2 randlocal400_col3 randlocal900_col3 randlocal1600_col3 randlocal2500_col3 randlocal400_col4 randlocal900_col4 randlocal1600_col4 randlocal2500_col4
do
    echo $file
    ./bin/autocorr -f $file --verbose --measurements=100 --iterations=1000000 --autocorr_terms=100 --start=0.4 --end=0.9 --metropolis --cluster
done

cd -