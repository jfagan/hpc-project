set term pngcairo enhanced
set out target

set grid

set xlabel "Processes"
set ylabel "Time"

set style line 5 lc rgb '#8b1a0e'  pt 1 ps 1 lt 1 lw 2 # --- red
set style line 4 lc rgb '#5e9c36'  pt 2 ps 1 lt 1 lw 2 # --- green
set style line 3 lc rgb '0x00008B' pt 3 ps 1 lt 1 lw 2 # --- pink
set style line 2 lc rgb '0xFF8C00'  pt 4 ps 1 lt 1 lw 2 # --- green
set style line 1 lc rgb '0x9932CC'  pt 5 ps 1 lt 1 lw 2 # --- green

set style line 11 lc rgb '#808080' lt 1
set border 3 back ls 11
set tics nomirror

plot "data/benchmark/unopt/".file w lp ls 5 t "Unoptimised", \
     "data/benchmark/streamlined/".file w lp ls 4 t "Streamlined", \
     "data/benchmark/vector/".file w lp ls 3 t "Replaced map with vector", \
     "data/benchmark/hidden_rand/".file w lp ls 2 t "Hidden rand generation", \
     "data/benchmark/lookup/".file w lp ls 1 t "Lookup table"
