#!/bin/bash

#SBATCH -N 4       
#SBATCH -p compute
#SBATCH -t 12:45:00  
#SBATCH -U mschpc
#SBATCH -J ising_benchmark
#SBATCH -o fournodes_randlocal2.out

# source the module commands
source /etc/profile.d/modules.sh

# load the modules used to build the xhpl binary
module load cports gcc/4.9.3-gnu openmpi/1.8.6-gnu4.9.3

procs=32

# run it
cd parallel

for size in 20000 
do
    for colour in  "_col3" "_col4"
    do
	for file in "randlocal${size}${colour}"
	do
	    target="data/strong_scaling/${file}_bitwise.dat"
	    echo $target
	    mpirun -n $procs bin/benchmark -f $file -i 100000 -v  -c 2 >>"../${target}"
	done
    done
done

for size in 25000 30000
do
    for colour in "_col2" "_col3" "_col4"
    do
	for file in "randlocal${size}${colour}"
	do
	    target="data/strong_scaling/${file}_bitwise.dat"
	    echo $target
	    mpirun -n $procs bin/benchmark -f $file -i 100000 -v  -c 2 >>"../${target}"
	done
    done
done

cd -