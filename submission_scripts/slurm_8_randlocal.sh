#!/bin/bash

#SBATCH -N 1       
#SBATCH -p compute
#SBATCH -t 17:00:00  
#SBATCH -U mschpc
#SBATCH -J ising_benchmark
#SBATCH -o onenode_randlocal.out

# source the module commands
source /etc/profile.d/modules.sh

# load the modules used to build the xhpl binary
module load cports gcc/4.9.3-gnu openmpi/1.8.6-gnu4.9.3

# run it
cd parallel

for size in 10000 15000 20000 25000 30000
do
    for colour in "_col2" "_col3" "_col4"
    do
	for file in "randlocal${size}${colour}"
	do
	    for procs in 1 2 4 8
	    do
		target="data/strong_scaling/${file}_bitwise.dat"
		echo $target
		mpirun -n $procs bin/benchmark -f $file -i 100000 -v  -c 2 >>"../${target}"
		
		target="data/strong_scaling/${file}_bitwisepersistent.dat"
		echo $target
		mpirun -n $procs bin/benchmark -f $file -i 100000 -v  -c 3 >>"../${target}"
	    done
	done
    done
done
cd -