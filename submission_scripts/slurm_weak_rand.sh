#!/bin/bash

#SBATCH -N 8       
#SBATCH -p compute
#SBATCH -t 13:00:00 
#SBATCH -U mschpc
#SBATCH -J ising_weak
#SBATCH -o weakrandlocal.out

# source the module commands
source /etc/profile.d/modules.sh

# load the modules used to build the xhpl binary
module load cports gcc/4.9.3-gnu openmpi/1.8.6-gnu4.9.3

# run it
cd parallel
for p in 1 2 4 8 16 32 64
do
    size=$((500*p))
    
    target="data/weak_scaling/randlocal_bitwise.dat"
    echo $target
    mpirun -n $p bin/benchmark -f "randlocal${size}_col2" -i 100000 -v  -c 2 >> "../${target}"
    
    target="data/weak_scaling/randlocal_bitwisepersistent.dat"
    echo $target
    mpirun -n $p bin/benchmark -f "randlocal${size}_col2" -i 100000 -v  -c 3 >>"../${target}"

done

cd -