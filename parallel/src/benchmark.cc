/*
  benchmark.cc -- parallel Ising model benchmarking
  Author       -- James Fagan (jfagan)
  Created      -- 09-06-2016
  Last updated -- Time-stamp: <2016-09-05 12:23:13 jfagan>
*/

#include "graph.h"
#include "ising.h"
#include "metropolis.h"
#include "update.h"

#include <getopt.h>
#include <mpi.h>
#include <string>

using namespace std;
using Parallel::IsingModel::Ising;
using Parallel::IsingModel::Metropolis;
using Parallel::IsingModel::NonBlocking;
using Parallel::IsingModel::Persistent;
using Parallel::IsingModel::Bitwise;
using Parallel::IsingModel::PersistentBitwise;

int process=0, nproc=0;

void print_usage();

int main(int argc, char** argv)
{
  srand48(9876789 + process);

  string filestub="";
  bool given_file = false;
  double beta=1;
  int iter = 1e5;
  bool verbose=false;
  bool rebalance=true;
  int comm_type=0; // default is regular non-blocking
  double start=0.0, end=0.0, max_elapsed=0.0;

  const struct option longopts[] =
    {
      {"file",         1,   0, 'f'},
      {"help",         0,   0, 'h'},
      {"verbose",      0,   0, 'v'},
      {"beta",         1,   0, 'b'},
      {"iterations",   1,   0, 's'},
      {"no-rebalance",    0,   0, 'r'},
      {"persistent",   0,   0, 'r'},
      {"comms",        1,   0, 'c'},
      {0,0,0,0},
    };

  MPI_Init( &argc, &argv );
  MPI_Comm_size( MPI_COMM_WORLD, &nproc );
  MPI_Comm_rank( MPI_COMM_WORLD, &process  );

  char opt;
  int index;
  while((opt=getopt_long(argc, argv, "f:b:i:c:hvr", longopts, &index)) != -1) {
    switch(opt) {
    case 'f':
      filestub = optarg; given_file = true; break;
    case 'b':
      beta = atof(optarg); break;
    case 'i':
      iter = atoi(optarg); break;
    case 'c':
      comm_type = atoi(optarg); break;
    case 'h':
      print_usage(); MPI_Abort(MPI_COMM_WORLD, 0); break;
    case 'v' :
      verbose = !process; break; // only be verbose on rank zero 
    case 'r':
      rebalance = false; break;
    default :
      cerr<<"Error: invalid option"<<endl;
      print_usage();
      MPI_Abort(MPI_COMM_WORLD, 1);
    }
  }

  if(!given_file && !process) {
    cout<<"--------------------------------"<<endl;
    cout<<" ERROR: no file specified."<<endl;
    cout<<"--------------------------------"<<endl;
    print_usage();
    MPI_Abort(MPI_COMM_WORLD, 1);
  }


  switch(comm_type) {
  case 0 :
    {
      start = MPI_Wtime();

      Ising<Metropolis, NonBlocking> ising_nb(filestub, rebalance);
      
      for(int j=0; j<iter; j++) {
	ising_nb.update(beta);
      }
  
      end = MPI_Wtime();
      break;
    }
  case 1 :
    {
      start = MPI_Wtime();

      Ising<Metropolis, Persistent>  ising_p(filestub, rebalance);
    
      for(int j=0; j<iter; j++) {
	ising_p.update(beta);
      }
    
      end = MPI_Wtime();
      break;
    }
  case 2 :
    {
      start = MPI_Wtime();

      Ising<Metropolis, Bitwise>  ising_bit(filestub, rebalance);
    
      for(int j=0; j<iter; j++) {
	ising_bit.update(beta);
      }
    
      end = MPI_Wtime();
      break;
    }
  case 3 :
    {
      start = MPI_Wtime();

      Ising<Metropolis, PersistentBitwise>  ising_pbit(filestub, rebalance);
    
      for(int j=0; j<iter; j++) {
	ising_pbit.update(beta);
      }
    
      end = MPI_Wtime();
      break;
    }
  default :
    if(!process) {
      cerr << "Invalid comms choice."<<endl;
      print_usage();
    }
    MPI_Abort(MPI_COMM_WORLD,1);
  }
  
  double elapsed = end-start;
  MPI_Reduce(&elapsed, &max_elapsed, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);

  if(verbose)
    cout<<nproc<<"\t"<<max_elapsed<<endl;

  MPI_Finalize();

  return 0;
}

void print_usage() {

  if(!process) {

    cout<<endl;
    cout<<" Parallel Ising model Benchmarking"<<endl;
    cout<<" by: James Fagan"<<endl;
    cout<<" This program times a number of Ising update steps, for benchmarking."<<endl;
    cout<<" Usage:"<<endl;
    cout<<" mpirun -n nproc benchmark [options]\n"<<endl;
    cout<<" GENERAL:"<<endl;
    cout<<"     -h, --help\n        prints this message\n"<<endl;
    cout<<"     -v, --verbose\n        activates verbose mode\n"<<endl;
    cout<<"     -r, --no-rebalance\n        don't use a heuristically rebalanced partition\n"<<endl;
    cout<<"     -f name, --file=name\n        specifies the graph file to run the model on\n"<<endl;
 
    cout<<" MEASUREMENTS:"<<endl;
    cout<<"     -b value, --beta=value\n        sets the temperature (beta) to value (default 1)\n"<<endl;
    cout<<"     -i value, --iterations=value\n        sets the number of iterations to average over (default 1E5)\n"<<endl;
    cout<<"     -c value, --comms=value\n        sets comms type. Nonblocking - 0, persistent - 1, bitarray - 2, persistent bitarray - 3 (default 0)\n"<<endl;
  }
}
