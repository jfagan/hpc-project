/*
  unittest_main.cc -- Main program containing tests for my parallel graph structure. Unit testing using wvtest in parallel, while it worked, wasn't great.
  Author       -- James Fagan (jfagan)
  Created      -- 29-06-2016
  Last updated -- Time-stamp: <2016-09-02 18:28:50 jfagan>
*/

#include "unittest.h"

#include "graph.h"
#include "update.h"

#include <mpi.h>
#include <cmath>
#include <string>
#include <unistd.h>

using namespace std;

using Parallel::Graph;
using Parallel::Test;
using Parallel::IsingModel::Bitwise;

int process=0, nproc=0;

void init(int argc, char **argv);
void finalize();

int main(int argc, char **argv)
{
  init(argc, argv);

  Graph<Bitwise> g("test");
  Test<Bitwise>::run_tests(g);

  finalize();

  return 0;
}

void init(int argc, char **argv) {
  srand48(9876789);

  MPI_Init( &argc, &argv );
  MPI_Comm_size( MPI_COMM_WORLD, &nproc );
  MPI_Comm_rank( MPI_COMM_WORLD, &process  );

  if(!process) {
    std::cout<<"\n\n";
    std::cout<<"================================================================================"<<std::endl;
    std::cout<<"                             PARALLEL UNIT TESTS                               "<<std::endl;
    std::cout<<"================================================================================"<<std::endl;
  }
}

void finalize() {
  
  if(!process) {
    std::cout<<"================================================================================"<<std::endl;
    std::cout<<"================================================================================"<<std::endl;
    std::cout<<"\n\n";
  }
  MPI_Finalize();
}
