/*
  isingmeasurements.cc -- main program for examining observbles of parallel Ising models on a graph
  Author       -- James Fagan (jfagan)
  Created      -- 09-06-2016
  Last updated -- Time-stamp: <2016-09-05 12:24:30 jfagan>
*/

#include "ising.h"
#include "metropolis.h"

#include <getopt.h>
#include <mpi.h>
#include <string>

using namespace std;
using Parallel::IsingModel::Ising;
using Parallel::IsingModel::Bitwise;
using Parallel::IsingModel::Metropolis;

int process=0, nproc=0;

void print_usage();

int main(int argc, char** argv)
{
  srand48(9876789 + process);

  string filestub="";
  bool given_file = false;
  double beta;
  double beta_start=0.05;
  double beta_end  = 1;
  int measurements = 10;
  int iter = 1e5;
  bool verbose=false;
  bool rebalance=true;

  const struct option longopts[] =
    {
      {"file",         1,   0, 'f'},
      {"help",         0,   0, 'h'},
      {"verbose",      0,   0, 'v'},
      {"no-rebalance", 0,   0, 'r'},
      {"start",        1,   0, 's'},
      {"end",          1,   0, 'e'},
      {"measurements", 1,   0, 'm'},
      {"iterations",   1,   0, 's'},
      {0,0,0,0},
    };

  MPI_Init( &argc, &argv );
  MPI_Comm_size( MPI_COMM_WORLD, &nproc );
  MPI_Comm_rank( MPI_COMM_WORLD, &process  );

  char opt;
  int index;
  while((opt=getopt_long(argc, argv, "f:s:e:i:m:hvr", longopts, &index)) != -1) {
    switch(opt) {
    case 'f':
      filestub = optarg; given_file = true; break;
    case 's':
      beta_start = atof(optarg); break;
    case 'e':
      beta_end = atof(optarg); break;
    case 'm':
      measurements = atoi(optarg); break;
    case 'i':
      iter = atoi(optarg); break;
    case 'h':
      print_usage(); MPI_Abort(MPI_COMM_WORLD, 0); break;
    case 'v' :
      verbose = !process; break;// only be verbose on rank zero 
    case 'r' :
      rebalance = false; break;
    default :
      cerr<<"Error: invalid option"<<endl;
      print_usage();
      MPI_Abort(MPI_COMM_WORLD, 1);
    }
  }

  if(!given_file && !process) {
    cout<<"--------------------------------"<<endl;
    cout<<" ERROR: no file specified."<<endl;
    cout<<"--------------------------------"<<endl;
    print_usage();
    MPI_Abort(MPI_COMM_WORLD, 1);
  }

  Ising<Metropolis, Bitwise> ising(filestub, rebalance);

  beta = beta_start;
  double delta = (beta_end - beta_start)/measurements;

  for(int i=0; i<measurements; i++) {
    beta += delta;
    if(verbose) cout<<i+1<<" of "<<measurements<<endl;
      
    for(int j=0; j<0.1*iter; j++)
      ising.update(beta);
      
    double result = 0.0;
      
    for(int j=0; j<iter; j++) {
      result += ising.susceptibility(beta);
      ising.update(beta);
    }
      
    if(!process) cout<<i<<"\t"<< beta <<"\t"<< result/iter <<endl;
  }

  MPI_Finalize();

  return 0;
}

void print_usage() {

  if(!process) {

    cout<<endl;
    cout<<" Parallel Ising model on a graph"<<endl;
    cout<<" by: James Fagan"<<endl;
    cout<<" This program implements the Ising model on a parallel graph structure, with the Metropolis algorithm."<<endl;
    cout<<" Usage:"<<endl;
    cout<<" mpirun -n nproc isingmeasurements [options]\n"<<endl;
    cout<<" GENERAL:"<<endl;
    cout<<"     -h, --help\n        prints this message\n"<<endl;
    cout<<"     -v, --verbose\n        activates verbose mode\n"<<endl;
    cout<<"     -f name, --file=name\n        specifies the graph file to run the model on\n"<<endl;
    cout<<"     -r, --no-rebalance\n        don't use a heuristically rebalanced partition\n"<<endl;

 
    cout<<" MEASUREMENTS:"<<endl;
    cout<<"     -s value, --start=value\n        sets the initial temperature (beta) to value (default 0.05)\n"<<endl;
    cout<<"     -e value, --end=value\n        sets the final temperature (beta) to value (default 1)\n"<<endl;
    cout<<"     -i value, --iterations=value\n        sets the number of iterations to average over (default 1E5)\n"<<endl;
    cout<<"     -m value, --measurements=value\n        sets the number of measurements between beta_start and beta_end (default 10)\n"<<endl;

  }
}
