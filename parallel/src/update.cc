// update policies for the parallel graph class

#include "update.h"

#include "mersenne.h"
#include "site.h"

#include <mpi.h>
#include <random>
#include <vector>

namespace Parallel {
  namespace IsingModel {
  
    void NonBlocking::update(int update_colour, 
			     std::vector< Site<T> > &local_sites,
			     const vec3D<int>  &halo_sending, 
			     const vec3D<int>  &halo_recving, 
			     vec3D<char> &halo_values_sending, 
			     vec3D<char> &halo_values_recving,
			     const vec2D<int>  &send_list,
			     const vec2D<int>  &recv_list,
			     vec2D<MPI_Request>  &send_req,
			     vec2D<MPI_Request>  &recv_req,
			     vec2D<MPI_Status>  &send_stat,
			     vec2D<MPI_Status>  &recv_stat,
			     vec2D<double>  &random_doubles) {

      // update the values that have to be sent
      // ie halo_values_send, using indices halo_sending
      for(int i: send_list[update_colour]) { 
	for(unsigned int j=0; j<halo_sending[i][update_colour].size(); j++) {
	  halo_values_sending[i][update_colour][j] = local_sites[ halo_sending[i][update_colour][j] ].value();
	}
      }

      int send_index=0, recv_index=0; // to keep track of position in Request arrays

      for(int i : send_list[update_colour]) { 
	// send halo data to process i
	MPI_Isend(halo_values_sending[i][update_colour].data(),  // send buffer
		  halo_values_sending[i][update_colour].size(),  // number of items sent
		  MPI_CHAR,                                      // send datatype
		  i,                                             // sending to...
		  0,                                             // tag
		  MPI_COMM_WORLD,                                // communicator
		  &send_req[update_colour][send_index++]); 
      }

      for(int i : recv_list[update_colour] ) {
	// recieve halo data from process i
	MPI_Irecv(halo_values_recving[i][update_colour].data(),  // recieve buffer
		  halo_values_recving[i][update_colour].size(),  // number of items recieved
		  MPI_CHAR,                                      // recv datatype
		  i,                                             // recieving from...
		  0,                                             // tag
		  MPI_COMM_WORLD,                                // communicator
		  &recv_req[update_colour][recv_index++]);    	      
      } 
  
      // generate random numbers for next iteration here
      for(double &i : random_doubles[update_colour]) {
        i = rand_double(mt());
      }

      MPI_Waitall( send_list[update_colour].size(), send_req[update_colour].data(), send_stat[update_colour].data() ); 
      MPI_Waitall( recv_list[update_colour].size(), recv_req[update_colour].data(), recv_stat[update_colour].data() ); 

      // update the values that were just received
      // updating halo_values_recv, using indices halo_recving
      for(int i : recv_list[update_colour]) {
	for(unsigned int j=0; j<halo_recving[i][update_colour].size(); j++) {
	  local_sites[ halo_recving[i][update_colour][j] ].value() = halo_values_recving[i][update_colour][j];
	}
      }
    }

    void Persistent::update(int update_colour, 
			    std::vector< Site<T> > &local_sites,
			    const vec3D<int>  &halo_sending, 
			    const vec3D<int>  &halo_recving, 
			    vec3D<char> &halo_values_sending, 
			    vec3D<char> &halo_values_recving,
			    const vec2D<int>  &send_list,
			    const vec2D<int>  &recv_list,
			    vec2D<MPI_Request>  &send_req,
			    vec2D<MPI_Request>  &recv_req,
			    vec2D<MPI_Status>  &send_stat,
			    vec2D<MPI_Status>  &recv_stat,
			    vec2D<double>  &random_doubles) {

      // update the values that have to be sent
      // ie halo_values_send, using indices halo_sending
      for(int i: send_list[update_colour]) { // change to "for i in sendlist"
	for(unsigned int j=0; j<halo_sending[i][update_colour].size(); j++) {
	  halo_values_sending[i][update_colour][j] = local_sites[ halo_sending[i][update_colour][j] ].value();
	}
      }

      for(unsigned int i=0; i<send_list[update_colour].size(); i++) { 
	// send halo data to process i
	MPI_Start(&send_req[update_colour][i]);
      }

      for(unsigned int i=0; i<recv_list[update_colour].size(); i++ ) {
	// recieve halo data from process i
	MPI_Start(&recv_req[update_colour][i]);
      } 

      // generate random numbers for next iteration here
      for(double &i : random_doubles[update_colour]) {
        i = rand_double(mt());
      }

      MPI_Waitall( send_list[update_colour].size(), send_req[update_colour].data(), send_stat[update_colour].data() ); 
      MPI_Waitall( recv_list[update_colour].size(), recv_req[update_colour].data(), recv_stat[update_colour].data() ); 

      // update the values that were just received
      // updating halo_values_recv, using indices halo_recving
      for(int i : recv_list[update_colour]) {
	for(unsigned int j=0; j<halo_recving[i][update_colour].size(); j++) {
	  local_sites[ halo_recving[i][update_colour][j] ].value() = halo_values_recving[i][update_colour][j];
	}
      }
    }


 void Bitwise::update(int update_colour, 
			 std::vector< Site<T> > &local_sites,
			 const vec3D<int>  &halo_sending, 
			 const vec3D<int>  &halo_recving, 
			 vec3D<char> &halo_values_sending, 
			 vec3D<char> &halo_values_recving,
			 const vec2D<int>  &send_list,
			 const vec2D<int>  &recv_list,
			 vec2D<MPI_Request>  &send_req,
			 vec2D<MPI_Request>  &recv_req,
			 vec2D<MPI_Status>  &send_stat,
			 vec2D<MPI_Status>  &recv_stat,
			 vec2D<double>  &random_doubles) {

      int index=0;
      // populate bit array to be sent
      for(int i: send_list[update_colour]) { 
	for(unsigned int j=0; j<halo_sending[i][update_colour].size(); j++) {
	  bit_send[update_colour][index][j/32] |= (local_sites[ halo_sending[i][update_colour][j] ].value() > 0) << (j%32);
	}
	index++;
      }
  
      int send_index=0, recv_index=0; // to keep track of position in Request arrays


      for(int i : send_list[update_colour]) { 
	// send halo data to process i
	MPI_Isend(bit_send[update_colour][send_index].data(),  // send buffer
		  bit_send[update_colour][send_index].size(),  // number of items sent
		  MPI_INT,                                      // send datatype
		  i,                                            // sending to...
		  process,                                      // tag
		  MPI_COMM_WORLD,                               // communicator
		  &send_req[update_colour][send_index]);

	send_index++;
      }

      for(int i : recv_list[update_colour] ) {
	// recieve halo data from process i
	MPI_Irecv(bit_recv[update_colour][recv_index].data(),                   // recieve buffer
		  bit_recv[update_colour][recv_index].size(),                   // number of items recieved
		  MPI_INT,                                       // recv datatype
		  i,                                             // recieving from...
		  i,                                             // tag
		  MPI_COMM_WORLD,                                // communicator
		  &recv_req[update_colour][recv_index]);

	recv_index++;
      } 

      // generate random numbers for next iteration here
      for(double &i : random_doubles[update_colour]) {
        i = rand_double(mt());
      }

      MPI_Waitall( send_list[update_colour].size(), send_req[update_colour].data(), send_stat[update_colour].data() ); 
      MPI_Waitall( recv_list[update_colour].size(), recv_req[update_colour].data(), recv_stat[update_colour].data() ); 

      // extract the updated values from the recved bit array
      index=0;
      for(int i : recv_list[update_colour]) {
      	for(unsigned int j=0; j<halo_recving[i][update_colour].size(); j++) {
      	  local_sites[ halo_recving[i][update_colour][j] ].value() = (bit_recv[update_colour][index][j/32] & 1<<(j%32) ) ? 1 : -1;
      	}
      	index++;
      }
    }


 void PersistentBitwise::update(int update_colour, 
			 std::vector< Site<T> > &local_sites,
			 const vec3D<int>  &halo_sending, 
			 const vec3D<int>  &halo_recving, 
			 vec3D<char> &halo_values_sending, 
			 vec3D<char> &halo_values_recving,
			 const vec2D<int>  &send_list,
			 const vec2D<int>  &recv_list,
			 vec2D<MPI_Request>  &send_req,
			 vec2D<MPI_Request>  &recv_req,
			 vec2D<MPI_Status>  &send_stat,
			 vec2D<MPI_Status>  &recv_stat,
			 vec2D<double>  &random_doubles) {

      int index=0;
      // populate bit array to be sent
      for(int i: send_list[update_colour]) { 
	for(unsigned int j=0; j<halo_sending[i][update_colour].size(); j++) {
	  bit_send[update_colour][index][j/32] |= (local_sites[ halo_sending[i][update_colour][j] ].value() > 0) << (j%32);
	}
	index++;
      }
  
      for(unsigned int i=0; i<send_list[update_colour].size(); i++) { 
	// send halo data to process i
	MPI_Start(&send_req[update_colour][i]);
      }

      for(unsigned int i=0; i<recv_list[update_colour].size(); i++ ) {
	// recieve halo data from process i
	MPI_Start(&recv_req[update_colour][i]);
      } 
  
      // generate random numbers for next iteration here
      for(double &i : random_doubles[update_colour]) {
        i = rand_double(mt());
      }

      MPI_Waitall( send_list[update_colour].size(), send_req[update_colour].data(), send_stat[update_colour].data() ); 
      MPI_Waitall( recv_list[update_colour].size(), recv_req[update_colour].data(), recv_stat[update_colour].data() ); 

      // extract the updated values from the recved bit array
      index=0;
      for(int i : recv_list[update_colour]) {
      	for(unsigned int j=0; j<halo_recving[i][update_colour].size(); j++) {
      	  local_sites[ halo_recving[i][update_colour][j] ].value() = (bit_recv[update_colour][index][j/32] & 1<<(j%32) ) ? 1 : -1;
      	}
      	index++;
      }
    }


    // doesn't do anything, but it'll still be called in the graph constructor so it needs to be defined
    void NonBlocking::initialise_update(int colours,
					vec3D<T>   &halo_values_sending, 
					vec3D<T>   &halo_values_recving,
					const vec2D<int> &send_list,
					const vec2D<int> &recv_list,
					vec2D<MPI_Request> &send_req,
					vec2D<MPI_Request> &recv_req) {}
    

    // size bit vectors
    void Bitwise::initialise_update(int colours,
				    vec3D<T>   &halo_values_sending, 
				    vec3D<T>   &halo_values_recving,
				    const vec2D<int> &send_list,
				    const vec2D<int> &recv_list,
				    vec2D<MPI_Request> &send_req,
				    vec2D<MPI_Request> &recv_req) {

      bit_send.resize(colours);
      bit_recv.resize(colours);
      
      for(int i=0; i<colours; i++) {
	bit_send[i].resize(send_list[i].size());
	bit_recv[i].resize(recv_list[i].size());
      
	// size recieving bit vector
	int index=0;
	for(int j : recv_list[i]) {
	  bit_recv[i][index++].resize((halo_values_recving[j][i].size()/32) + 1, 0);
	}

	index=0;
	for(int j : send_list[i]) {
	  bit_send[i][index++].resize((halo_values_sending[j][i].size()/32) + 1, 0);
	}

	for(auto &vec : bit_send[i]) { 
	  for(auto &k   : vec) {
	    k = 0; // set all bits to zero initially
	  }
	}

	for(auto &vec : bit_recv[i]) { 
	  for(auto &k   : vec) {
	    k = 0; // set all bits to zero initially
	  }
	}

      }
    }

    // sets up persistent comms
    void Persistent::initialise_update(int colours,
				       vec3D<T>   &halo_values_sending, 
				       vec3D<T>   &halo_values_recving,
				       const vec2D<int> &send_list,
				       const vec2D<int> &recv_list,
				       vec2D<MPI_Request> &send_req,
				       vec2D<MPI_Request> &recv_req) {
      
      for(int i=0; i<colours; i++) {

	int send_index=0, recv_index=0;

	for(int j : send_list[i])
	  MPI_Send_init(halo_values_sending[j][i].data(), halo_values_sending[j][i].size(), MPI_CHAR, j, process, MPI_COMM_WORLD, &send_req[i][send_index++]);

	for(int j : recv_list[i])
	  MPI_Recv_init(halo_values_recving[j][i].data(), halo_values_recving[j][i].size(), MPI_CHAR, j, j,       MPI_COMM_WORLD, &recv_req[i][recv_index++]);
      }
    }


    // size bit vectors
    void PersistentBitwise::initialise_update(int colours,
				    vec3D<T>   &halo_values_sending, 
				    vec3D<T>   &halo_values_recving,
				    const vec2D<int> &send_list,
				    const vec2D<int> &recv_list,
				    vec2D<MPI_Request> &send_req,
				    vec2D<MPI_Request> &recv_req) {

      bit_send.resize(colours);
      bit_recv.resize(colours);
      
      for(int i=0; i<colours; i++) {
	bit_send[i].resize(send_list[i].size());
	bit_recv[i].resize(recv_list[i].size());
      
	// size recieving bit vector
	int index=0;
	for(int j : recv_list[i]) {
	  bit_recv[i][index++].resize((halo_values_recving[j][i].size()/32) + 1, 0);
	}

	index=0;
	for(int j : send_list[i]) {
	  bit_send[i][index++].resize((halo_values_sending[j][i].size()/32) + 1, 0);
	}

	for(auto &vec : bit_send[i]) { 
	  for(auto &k   : vec) {
	    k = 0; // set all bits to zero initially
	  }
	}

	for(auto &vec : bit_recv[i]) { 
	  for(auto &k   : vec) {
	    k = 0; // set all bits to zero initially
	  }
	}
      }

    for(int i=0; i<colours; i++) {

	int send_index=0, recv_index=0;

	for(int j : send_list[i]) {
	  MPI_Send_init(bit_send[i][send_index].data(),bit_send[i][send_index].size(), MPI_INT, j, process, MPI_COMM_WORLD, &send_req[i][send_index]);
	  send_index++;
	}

	for(int j : recv_list[i]) {
	  MPI_Recv_init(bit_recv[i][recv_index].data(),bit_recv[i][recv_index].size(), MPI_INT, j, j, MPI_COMM_WORLD, &recv_req[i][recv_index]);
	  recv_index++;
	}
      }
    }

  }
}


