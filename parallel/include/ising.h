/*
  ising.h -- header for parallel graph ising model
  Author       -- James Fagan (jfagan)
  Created      -- 04-07-2016
  Last updated -- Time-stamp: <2016-09-02 17:15:17 jfagan>
*/

#ifndef __PARALLEL_ISINGGRAPH_H__
#define __PARALLEL_ISINGGRAPH_H__

#include "graph.h"

#include <string>
#include <vector>

namespace Parallel {
  namespace IsingModel {

  template<template<class> class Algorithm, typename Update>
  class Ising : private Algorithm<Update> {

    typedef typename Algorithm<Update>::update_type update_type;
    using Algorithm<Update>::initialise;
    using Algorithm<Update>::perform_sweep;
    using Algorithm<Update>::measure_susceptibility;
  
    Graph<update_type> host;

    typedef typename std::vector<int>::iterator iterator;
    typedef typename std::vector<int>::const_iterator const_iterator;

  public:
    Ising(const Graph<update_type> &g);
    Ising(const std::string &filestub, bool rebalance);

    inline const int size() const { return host.size();    }

    inline void update(const double &beta) { perform_sweep(host, beta); }
    inline double susceptibility(const double &beta) { return measure_susceptibility(host, beta); }

    // iterators over site neighbours
    inline iterator begin(int colour) { return host.begin(colour); }
    inline iterator end  (int colour) { return host.end(colour);   }

    inline const_iterator cbegin(int colour) const { return host.cbegin(colour); }
    inline const_iterator cend  (int colour) const { return host.cend(colour);   }
  };

  #include "tpp/isinggraph.tpp"
}
}

// template<typename Algorithm>
// double magnetisation(Ising<Algorithm> &sites) {
//   int local_mag=0;
//   int global_mag=0;

//   // we still have to iterate one colour at a time
//   // although it doesn't matter here since we aren't flipping sites
//   for(int i=0; i<sites.total_colours(); i++) {
//     for(auto it = sites.begin(i); it!=sites.end(i); ++it) {
//       local_mag += sites[*it];
//     }
//   }

//   // send result only to rank 0 - we'll wan to output to file so allreduce not necessary
//   MPI_Reduce(&local_mag, &global_mag, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);

//   return (double)global_mag/sites.size();
// }

#endif


