
#ifndef __PARALLEL_METROPOLIS_H__
#define __PARALLEL_METROPOLIS_H__

#include "graph.h"
#include "mersenne.h"
#include "update.h"

#include <random>
#include <vector>
 
namespace Parallel {
  namespace IsingModel {
    
    template<typename Update>
    class Metropolis {
    protected:
      typedef Update update_type;

    private:
      int max_degree;
      std::vector< std::vector<double> > flip_probs;
      std::uniform_real_distribution<double> rand_double;
      int energy              (const ::Parallel::Graph<update_type> &g, int site) const;
      int agreeing_neighbours (const ::Parallel::Graph<update_type> &g, int site) const;

    protected:

      void   initialise            (::Parallel::Graph<update_type> &g);
      void   perform_sweep         (::Parallel::Graph<update_type> &g, const double &beta);
      double measure_susceptibility(::Parallel::Graph<update_type> &g, const double &beta);
    };

    #include "tpp/metropolis.tpp"
  }
}

#endif
