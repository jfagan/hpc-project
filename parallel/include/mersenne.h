/*
  mersenne.h -- random number generation
  Author       -- James Fagan (jfagan)
  Created      -- 27-07-2016
  Last updated -- Time-stamp: <2016-07-27 17:51:46 jfagan>
*/

#ifndef __MERSENNE_H__
#define __MERSENNE_H__

#include <random>

extern int process, nproc;

inline std::mt19937 & mt() {
  static std::mt19937 e(989*(process+1)); // different random numbers on each process
  return e;
}

#endif
