#ifndef __MYTEST_H__
#define __MYTEST_H__

#include "graph.h"
#include "update.h"

#include <chrono>
#include <iostream>
#include <mpi.h>
#include <thread>
#include <string>

namespace Parallel {

  template<typename Update>
  class Test {
  public:
    static void run_tests(Graph<Update> &g);
  };

#include "tpp/unittest.tpp"
}

#endif
