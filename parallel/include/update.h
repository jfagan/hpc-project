/*
  update.h -- various halo update methods for the graph class
  Author       -- James Fagan (jfagan)
  Created      -- 19-08-2016
  Last updated -- Time-stamp: <2016-09-02 17:21:49 jfagan>
*/

#ifndef __UPDATE_H__
#define __UPDATE_H__

#include "site.h"

#include <mpi.h>
#include <random>
#include <unistd.h>
#include <vector>

extern int process, nproc;

namespace Parallel {
  namespace IsingModel {

    class NonBlocking {

      template <typename U>
      using vec3D = std::vector< std::vector< std::vector<U> > >;

      template <typename U>
      using vec2D = std::vector< std::vector<U> >;

      std::uniform_real_distribution<double> rand_double;
    
    protected:

      NonBlocking() : rand_double{0.0, 1.0} {}

      typedef char T;

      void update(int update_colour, 
		  std::vector< Site<T> > &local_sites,
		  const vec3D<int>  &halo_sending, 
		  const vec3D<int>  &halo_recving, 
		  vec3D<T> &halo_values_sending, 
		  vec3D<T> &halo_values_recving,
		  const vec2D<int>  &send_list,
		  const vec2D<int>  &recv_list,
		  vec2D<MPI_Request>  &send_req,
		  vec2D<MPI_Request>  &recv_req,
		  vec2D<MPI_Status>  &send_stat,
		  vec2D<MPI_Status>  &recv_stat,
		  vec2D<double>  &random_doubles);

      void initialise_update(int colours,
			     vec3D<T> &halo_values_sending, 
			     vec3D<T> &halo_values_recving,
			     const vec2D<int> &send_list,
			     const vec2D<int> &recv_list,
			     vec2D<MPI_Request> &send_req,
			     vec2D<MPI_Request> &recv_req);

    };

    class Persistent {

      template <typename U>
      using vec3D = std::vector< std::vector< std::vector<U> > >;

      template <typename U>
      using vec2D = std::vector< std::vector<U> >;
    
      std::uniform_real_distribution<double> rand_double;
    
    protected:

      Persistent() : rand_double{0.0, 1.0} {}
    
      typedef char T;


      void update(int update_colour,
		  std::vector< Site<T> > &local_sites,
		  const vec3D<int>  &halo_sending, 
		  const vec3D<int>  &halo_recving, 
		  vec3D<T> &halo_values_sending, 
		  vec3D<T> &halo_values_recving,
		  const vec2D<int>  &send_list,
		  const vec2D<int>  &recv_list,
		  vec2D<MPI_Request>  &send_req,
		  vec2D<MPI_Request>  &recv_req,
		  vec2D<MPI_Status>  &send_stat,
		  vec2D<MPI_Status>  &recv_stat,
		  vec2D<double>  &random_doubles);


      void initialise_update(int colours,
			     vec3D<T> &halo_values_sending, 
			     vec3D<T> &halo_values_recving,
			     const vec2D<int> &send_list,
			     const vec2D<int> &recv_list,
			     vec2D<MPI_Request> &send_req,
			     vec2D<MPI_Request> &recv_req);
    };

    class Bitwise {

      template <typename U>
      using vec3D = std::vector< std::vector< std::vector<U> > >;

      template <typename U>
      using vec2D = std::vector< std::vector<U> >;

      std::uniform_real_distribution<double> rand_double;
    
    protected:

      Bitwise() : rand_double{0.0, 1.0} {}

      typedef char T;

      vec3D<unsigned int> bit_send; // first index is colour, second process, third data
      vec3D<unsigned int> bit_recv;

      void update(int update_colour, 
		  std::vector< Site<T> > &local_sites,
		  const vec3D<int>  &halo_sending, 
		  const vec3D<int>  &halo_recving, 
		  vec3D<T> &halo_values_sending, 
		  vec3D<T> &halo_values_recving,
		  const vec2D<int>  &send_list,
		  const vec2D<int>  &recv_list,
		  vec2D<MPI_Request>  &send_req,
		  vec2D<MPI_Request>  &recv_req,
		  vec2D<MPI_Status>  &send_stat,
		  vec2D<MPI_Status>  &recv_stat,
		  vec2D<double>  &random_doubles);

      void initialise_update(int colours,
			     vec3D<T> &halo_values_sending, 
			     vec3D<T> &halo_values_recving,
			     const vec2D<int> &send_list,
			     const vec2D<int> &recv_list,
			     vec2D<MPI_Request> &send_req,
			     vec2D<MPI_Request> &recv_req);

    };


    class PersistentBitwise {

      template <typename U>
      using vec3D = std::vector< std::vector< std::vector<U> > >;

      template <typename U>
      using vec2D = std::vector< std::vector<U> >;

      std::uniform_real_distribution<double> rand_double;
    
    protected:

      PersistentBitwise() : rand_double{0.0, 1.0} {}

      typedef char T;

      vec3D<unsigned int> bit_send; // first index is colour, second process, third data
      vec3D<unsigned int> bit_recv;

      void update(int update_colour, 
		  std::vector< Site<T> > &local_sites,
		  const vec3D<int>  &halo_sending, 
		  const vec3D<int>  &halo_recving, 
		  vec3D<T> &halo_values_sending, 
		  vec3D<T> &halo_values_recving,
		  const vec2D<int>  &send_list,
		  const vec2D<int>  &recv_list,
		  vec2D<MPI_Request>  &send_req,
		  vec2D<MPI_Request>  &recv_req,
		  vec2D<MPI_Status>  &send_stat,
		  vec2D<MPI_Status>  &recv_stat,
		  vec2D<double>  &random_doubles);

      void initialise_update(int colours,
			     vec3D<T> &halo_values_sending, 
			     vec3D<T> &halo_values_recving,
			     const vec2D<int> &send_list,
			     const vec2D<int> &recv_list,
			     vec2D<MPI_Request> &send_req,
			     vec2D<MPI_Request> &recv_req);

    };


  }
}
#endif
