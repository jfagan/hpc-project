/*
  A class to organise the metadata at each site of a graph
*/

#ifndef __SITE_H__
#define __SITE_H__

#include <algorithm>
#include <exception>
#include <stdexcept>
#include <vector>

namespace Parallel {

  template<typename T>
  class Site {
    T val;
    int col; // colour
    int part; // partition
    std::vector<int> neigh;
  
    typedef typename std::vector<int>::iterator iterator;
    typedef typename std::vector<int>::const_iterator const_iterator;
  
  public:
    Site() :
      val{0}, col{-1}, part{-1} {} // dummy site
  
    Site(const std::vector<int> &n, int colour, int partition) :
      val{0}, col{colour}, part{partition}, neigh{n} {}

    void operator=(const Site<T> &n) {
      val   = n.value();
      col = n.colour();
      part = n.partition();
      neigh = n.neighbours();
    }

    // private member access
    const T& value() const { return val; }
    T&       value()       { return val; }
  
    // site properties
    inline const int               degree    ()      const { return neigh.size(); }
    //    inline const int              label     ()      const { return lab;          }
    inline const int               colour    ()      const { return col;          }
    inline const int               partition ()      const { return part;         }
    inline const int               neighbour (int i) const { return neigh[i];     }
    inline const std::vector<int> &neighbours()      const { return neigh;        }

    // check if a certain site is a neighbour
    bool is_neighbour(int i) {
      return ( std::find( this->neigh_cbegin(), this->neigh_cend(), i) != this->neigh_cend() );
    }
  
    // add a neighbour
    void add_neigh(int site_label) {
      if(!is_neighbour( site_label )) {
	neigh.push_back( site_label );
      }
    }

    // remove a neighbour
    void del_neigh(int site_label) {
      iterator position = find( neigh.begin(), neigh.end(), site_label);
      if( position == neigh.end() ) {
	throw std::out_of_range("Trying to remove non-existent neighbour!");
      } else {
	neigh.erase( position ); 
      }
    }

    // non-const iterators over neighbours
    iterator neigh_begin() { return neigh.begin(); }
    iterator neigh_end()   { return neigh.end();   }

    // const iterators over neighbours
    const_iterator neigh_begin() const { return neigh.cbegin(); }
    const_iterator neigh_end()   const { return neigh.cend();   }
  };
}

#endif

