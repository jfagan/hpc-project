
template<typename Update>
int Metropolis<Update>::agreeing_neighbours(const ::Parallel::Graph<Update> &g, int site) const {
  int agree=0;
  for(auto &i : g[site].neighbours()) {
    agree += !(g[i].value()-g[site].value());
  }
  return agree;
}

template<typename Update>
int Metropolis<Update>::energy(const ::Parallel::Graph<Update> &g, int site) const {
  int total=0;
  for(int i : g[site].neighbours()) {
    total += g[i].value();
  }
  return -total*g[site].value();
}

template<typename Update>
void Metropolis<Update>::initialise(::Parallel::Graph<Update> &g) {
  max_degree = 0;
            
  for(int i=0; i<g.total_colours(); i++) { // consider sites one colour at a time
    for(auto it = g.begin(i); it != g.end(i); ++it) {
      if(g[*it].degree() > max_degree) max_degree = g[*it].degree();

      g[*it].value() = 1;
      if(drand48() < 0.5) // drand() is good enough here
	g[*it].value() = -1;
    }
  } 

  flip_probs.resize(max_degree+1);

  for(int i=1; i<=max_degree; i++) {
    flip_probs[i].resize(i+1);
  }
}

template<typename Update>
void Metropolis<Update>::perform_sweep(::Parallel::Graph<Update> &g, const double &beta) {

  // calculate lookup table of flip probabilities
  for(int i=1; i<=max_degree; i++) {
    // i is the degree
    int neighbour_sum  = -i;
    for(int j=0; j<=i; j++) { 
      flip_probs[i][j] = exp(-2*neighbour_sum*beta);
      neighbour_sum += 2;
    }
  }
  
  for(int i=0; i<g.total_colours(); i++) { // consider sites one colour at a time
    int index=0;
    for(auto it = g.begin(i); it != g.end(i); ++it) {
	  
      // double acceptance = exp(2*beta*( energy(g, *it) ));    
      double acceptance = flip_probs[g[*it].degree()][agreeing_neighbours(g,*it)];
	  
      if( acceptance > g.rand(i,index++) ) {
	g[*it].value() *= -1;
      }
    }
    g.update_halo(i); // only need to send the values that have changed - colour i
  }
}

template<typename Update>
double Metropolis<Update>::measure_susceptibility(::Parallel::Graph<Update> &g, const double &beta) {

  int n=g.size();
  double avg_mag=0;
  int mag=0;
  int total_mag=0;
      
  for(int i=0; i<g.total_colours(); i++) { // consider sites one colour at a time
    for(auto it = g.begin(i); it != g.end(i); ++it) {
      mag += g[*it].value();
    }
  }

  MPI_Reduce(&mag, &total_mag, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);

  avg_mag = total_mag/(double)n;
     
  return avg_mag*avg_mag;
}
 
