
void check(std::string message, bool pass) {
  // make sure that all processes are in the same test at the same time:
  
  bool abort_flag = false;
  
  for(int i=0; i<nproc; i++) {
    if(!pass && process == i) {    
      std::cout<<" > "<<message<<" ... ";
      std::cout<< "\033[1;31mFAILED\033[0m on rank "<<process<<std::endl;
      abort_flag = true;
      
      // keep the messages in a sensible order
      std::this_thread::sleep_for(std::chrono::milliseconds(200));
    } 

  }

  if(abort_flag) {
    if(!process) {
      std::cout<<"\nAborting... (Any error is likely to be fatal)"<<std::endl;
      std::cout<<"=============================================================================="<<std::endl;
      std::cout<<"=============================================================================="<<std::endl;
      std::cout<<"\n\n";
    }
    MPI_Abort(MPI_COMM_WORLD, 1);
  }
 
  if(!process) {
    // std::cout<<"ok"<<std::endl;
    std::cout<<" > "<<message<<" ... ";
    std::cout<< "\033[1;32mok\033[0m" <<std::endl;
  }
}

template<typename Update>
void Parallel::Test<Update>::run_tests(Parallel::Graph<Update> &g) {
  
  /**********************************************************************************/
  {
    check("Checking the testing framework", 1);
    MPI_Barrier(MPI_COMM_WORLD);
  }
  /**********************************************************************************/
  {
    int size = g.size();
    int sizes[nproc];
    MPI_Allgather(&size, 1, MPI_INT, sizes, 1, MPI_INT, MPI_COMM_WORLD);

    bool flag = true;
    for(int i : sizes)
      flag &= i==size;
 
    check("Global sizes on each process match", flag);
    MPI_Barrier(MPI_COMM_WORLD);
  } 
  /**********************************************************************************/
  {
    int colours = g.total_colours();
    int all_colours[nproc];
    MPI_Allgather(&colours, 1, MPI_INT, all_colours, 1, MPI_INT, MPI_COMM_WORLD);

    bool flag = true;
    for(int i : all_colours)
      flag &= i==colours;

    check("Total colours on each process match", flag);
    MPI_Barrier(MPI_COMM_WORLD);
  }
  /**********************************************************************************/
  {
    // check that the sum of the local sizes is the global size

    int test_global_size;
    int local_size = g.local_size();
    MPI_Reduce(&local_size, &test_global_size, 1, MPI_INT, MPI_SUM, 0,MPI_COMM_WORLD);

    if(process==0) {
      check("Sum of local sizes equal to global size", test_global_size == g.size());
    }

    MPI_Barrier(MPI_COMM_WORLD);
  }
  /**********************************************************************************/
  {
    // check that all the locals know that they're local
    int locals_are_local = true;

    for(int i=0; i<g.total_colours(); i++) {
      for(int j : g.local_owned[i]  ) {
  	locals_are_local &= (g[j].partition() == process);
      }
    }

    check("Local sites on each process store the correct owning process", locals_are_local);
    MPI_Barrier(MPI_COMM_WORLD);
  } 
  /**********************************************************************************/
  {
    // check that the halo buffer sizes match up internally on each process
    // should be sending the same number of values to rank X as we are receiving,
    // since the graph is undirected.

    bool halo_flag_internal = true;

    for(int i=0; i<nproc; i++) {
      halo_flag_internal &= (g.halo_recving[i].size() == g.halo_sending[i].size());
    }

    check("Halo in/out buffer sizes are internally consistent on each process", halo_flag_internal);
    MPI_Barrier(MPI_COMM_WORLD);

  }
  /**********************************************************************************/
  {
    int my_halo_sizes[nproc];    // halo sizes on my rank, to be sent to other ranks
    unsigned int halo_compare; // to store halo sizes from other ranks and compare to my own
    bool halo_flag_external = true;
    
    for(int i=0; i<nproc; i++) {
      my_halo_sizes[i] = g.halo_sending[i].size();
    }
  
    for(int i=0; i<nproc; i++) {
      MPI_Scatter(my_halo_sizes, 1, MPI_INT, &halo_compare, 1, MPI_INT, i, MPI_COMM_WORLD);
    
      halo_flag_external &= (halo_compare == g.halo_recving[i].size());			   
    }

    check("Halo in/out buffer sizes are consistent between processes", halo_flag_external);  
    MPI_Barrier(MPI_COMM_WORLD);
  }
  /**********************************************************************************/
  {
    bool halo_flag = true;
    bool correct_update_flag =true;
    bool colour_only_update = true;

    // we can do the following since we use integer types
    // nothing like Ising, but convenient for testing
    for(int i=0; i<g.total_colours(); i++) {
      for(int j : g.local_owned[i]) {
  	g[j].value() = (process%2) ? 1 : -1;
      }
    }

    // check that all the halo nodes still have value zero
    for(auto const &iter : g.local_sites) {
      if( iter.partition() != process && iter.partition() != -1) {
    	halo_flag &= (iter.value() == 0);
      } 
    }

    for(int i=0; i<g.total_colours(); i++) {
      g.update_halo(i);

      // make sure we're updating the coloured halos as expected
      for(auto const &iter : g.local_sites) {
      	if( iter.partition() != process && iter.colour()>i ) {
      	  colour_only_update &= (iter.value() == 0);
      	} 
      }
    }

    // now... check that all the sites on every process that have different values are halo nodes that are owned by an appropriate process    
    for(auto const &iter : g.local_sites ) {
      if(iter.partition() != -1) // not a dummy site
	correct_update_flag &= (iter.value() == ( (iter.partition()%2) ? 1 : -1));
    }
    
    // check local sites still have correct values
    for(int i=0; i<g.total_colours(); i++) {
      for(int j : g.local_owned[i]) {
	correct_update_flag &= (g[j].value() == ( (process%2) ? 1 : -1) );	    
      }
    }
  
    check("Halo nodes not accessible through the local owned list", halo_flag);
    MPI_Barrier(MPI_COMM_WORLD);
  
    check("Halo nodes are being updated one colour at a time", colour_only_update);
    MPI_Barrier(MPI_COMM_WORLD);

    check("Halo updates communicating the correct values", correct_update_flag);
    MPI_Barrier(MPI_COMM_WORLD);

  }
  /**********************************************************************************/
}


