
template <template<class> class Algorithm, typename Update>
Ising<Algorithm, Update>::Ising(const Graph<update_type> &g) : host{g} {
  initialise(host);
}

template <template<class> class Algorithm, typename Update>
Ising<Algorithm, Update>::Ising(const std::string &filestub, bool rebalance) : host{filestub, rebalance} {
  initialise(host);
}
