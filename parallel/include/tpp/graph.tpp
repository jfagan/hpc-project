
// copy constructor
template<typename Update>
Graph<Update>::Graph(const Graph<Update> &g) : rand_double{0.0, 1.0} {
  this->colours      = g.colours;
  this->size_global  = g.size_global;
  this->halo_recving = g.halo_recving;
  this->halo_sending = g.halo_sending;
  this->local_sites  = g.local_sites;
  this->local_owned  = g.local_owned;
}

// graph file constructor
template<typename Update>
Graph<Update>::Graph(const std::string & filestub, bool rebalance) : rand_double{0.0, 1.0} {

  std::vector<int> site_labels;
  std::vector<int> site_colours;
  std::vector<int> num_of_neighbours;
  std::vector<int> neighbour_labels;

  if (!process) {

    vec2D< Site<T> > sites                  ( nproc, std::vector< Site<T> >(0) );
    vec2D<   int   > all_site_labels        ( nproc, std::vector< int >(0,0)   );
    vec2D<   int   > all_site_colours       ( nproc, std::vector< int >(0,0)   );
    vec2D<   int   > all_num_of_neighbours  ( nproc, std::vector< int >(0,0)   );
    vec2D<   int   > all_neighbour_labels   ( nproc, std::vector< int >(0,0)   );
   
    // fill these arrays with the relevant information from the files
    read_file_data(filestub, rebalance, sites, all_site_labels, all_site_colours, all_num_of_neighbours, all_neighbour_labels);

    // keep process 0's data here
    site_labels  = all_site_labels[0];
    site_colours = all_site_colours[0];
    num_of_neighbours = all_num_of_neighbours[0];
    neighbour_labels = all_neighbour_labels[0];
  
    // send to the rest of the processes
    for(int i=1; i<nproc; i++) {
      send_file_data(i, all_site_labels, all_site_colours, all_num_of_neighbours, all_neighbour_labels);
    }

    for(int i=0; i<nproc; i++) {
      vec3D<int> halo_sending_setup (nproc, vec2D<int>(this->colours, std::vector<int>(0,0)));
      vec3D<int> halo_recving_setup (nproc, vec2D<int>(this->colours, std::vector<int>(0,0)));

      // set up the halo data to be sent, then send it
      init_halo_data(i, sites, all_site_labels, all_site_colours, halo_sending_setup, halo_recving_setup);
      send_halo_data(i, halo_sending_setup, halo_recving_setup);
    }
    
  } else {
    // unpack all of the info from process zero
    unpack_recv_data(site_labels, site_colours, num_of_neighbours, neighbour_labels);
  }

  // Construct the local_site maps on each process
  init_local_sites(site_labels, site_colours, num_of_neighbours, neighbour_labels);

  // size the value arrays
  halo_values_sending.resize(halo_sending.size());
  halo_values_recving.resize(halo_recving.size());

  for(unsigned int i=0; i<halo_sending.size(); i++) {
    halo_values_sending[i].resize(halo_sending[i].size());
    for(unsigned int j=0; j<halo_sending[i].size(); j++) {
      halo_values_sending[i][j].resize(halo_sending[i][j].size());
    }
  }

  for(unsigned int i=0; i<halo_recving.size(); i++) {
    halo_values_recving[i].resize(halo_recving[i].size());
    for(unsigned int j=0; j<halo_recving[i].size(); j++) {
      halo_values_recving[i][j].resize(halo_recving[i][j].size());
    }
  }

  // set up send and recv lists, send/recv requests, send/recv statuses

  send_list.resize(colours);
  recv_list.resize(colours);

  send_req.resize(colours);
  recv_req.resize(colours);

  send_stat.resize(colours);
  recv_stat.resize(colours);

  for(int i=0; i<nproc; i++) {
    for(int j=0; j<colours; j++) {
      if(halo_sending[i][j].size() > 0)
  	send_list[j].push_back(i);
      if(halo_recving[i][j].size() > 0)
  	recv_list[j].push_back(i);
    }
  }

  for(int i=0; i<colours; i++) {
    send_req[i].resize(send_list[i].size());
    recv_req[i].resize(recv_list[i].size());
    
    send_stat[i].resize(send_list[i].size());
    recv_stat[i].resize(recv_list[i].size());
  }
  
  initialise_update(colours, halo_values_sending, halo_values_recving, send_list, recv_list, send_req, recv_req);
}

// utility function for graph file constructor
template<typename Update>
void Graph<Update>::read_file_data(const std::string & filestub, 
			      bool rebalance,
			      vec2D< Site<T> > & sites,
			      vec2D< int > & all_site_labels,
			      vec2D< int > & all_site_colours,
			      vec2D< int > & all_num_of_neighbours,
			      vec2D< int > & all_neighbour_labels ) {
  
  ::std::string adjacency_filename = "../adj_files/" + filestub + ".adj";
  ::std::string partition_filename;

  if(rebalance)
    partition_filename = "../partition_files/" + filestub + ".partition" + ::std::to_string(nproc);
  else 
    partition_filename = "../partition_files/" + filestub + "_unbalanced.partition" + ::std::to_string(nproc);

  std::ifstream in_partition, in_adjacency;

  in_partition.open(partition_filename.c_str(), std::ios::in);
  in_adjacency.open(adjacency_filename.c_str(), std::ios::in);

  if(!in_partition.is_open() )
    throw std::ios_base::failure("Failed to open partition file.");
  else if(!in_adjacency.is_open() )
    throw std::ios_base::failure("Failed to open adjacency file.");

  std::string line, item; 

  getline(in_partition, line);
  std::stringstream(line) >> this->size_global;

  getline(in_adjacency, line);
  int test;
  std::stringstream(line) >> test;

  if( test != this->size_global) {
    throw std::logic_error("Incompatible graph sizes given from partition and adjacency files");
  }

  getline(in_adjacency, line);	
  std::stringstream(line) >> this->colours;
  
  for(int i=0; i<this->size_global; i++) { 
    getline (in_partition, line);
    int index;
    std::stringstream(line) >> index;

    getline(in_adjacency,line);
    std::stringstream ss(line);
    std::vector<int> neighbours;
    
    std::getline(ss,item,',');
    int site_colour;
    std::stringstream(item) >> site_colour;

    int sum=0;
    
    while(std::getline(ss,item,',')) {
      int neigh_label;
      std::stringstream(item) >> neigh_label ;
      neighbours.push_back(neigh_label);
      all_neighbour_labels[ index ].push_back(neigh_label);
      sum++; 
    }
    
    //    std::cout<<index<<" "<<i<<std::endl;

    //                         neighbour vector, colour, process
    Site<T> new_site = Site<T>(neighbours, site_colour, index);
    sites[ index ].push_back( new_site );

    // this data is for sending
    all_num_of_neighbours [ index ].push_back( sum         );
    all_site_colours      [ index ].push_back( site_colour );
    all_site_labels       [ index ].push_back( i           );
  }
  
  in_partition.close();
  in_adjacency.close();
}

// utility function for graph file constructor
template<typename Update>
void Graph<Update>::send_file_data( const int send_to_process, 
			       const vec2D< int > & all_site_labels,
			       const vec2D< int > & all_site_colours,
			       const vec2D< int > & all_num_of_neighbours,
			       const vec2D< int > & all_neighbour_labels) {
  
  int info_buffer[3];
  
  info_buffer[0] = all_site_labels[ send_to_process ].size(); // number of items
  info_buffer[1] = this->colours;
  info_buffer[2] = this->size_global;

  MPI_Ssend( info_buffer, 3, MPI_INT, send_to_process, 0, MPI_COMM_WORLD );
  
  // now send most of the other data

  MPI_Ssend( (void *)&all_site_labels       [send_to_process][0], info_buffer[0],             MPI_INT, send_to_process, 1, MPI_COMM_WORLD); 
  MPI_Ssend( (void *)&all_site_colours      [send_to_process][0], info_buffer[0],             MPI_INT, send_to_process, 2, MPI_COMM_WORLD); 
  MPI_Ssend( (void *)&all_num_of_neighbours [send_to_process][0], info_buffer[0],             MPI_INT, send_to_process, 3, MPI_COMM_WORLD); 
  MPI_Ssend( (void *)&all_neighbour_labels  [send_to_process][0], all_neighbour_labels[send_to_process].size(), MPI_INT, send_to_process, 4, MPI_COMM_WORLD);

}

// utility function for graph file constructor
template<typename Update>
void Graph<Update>::init_halo_data(const int which_process,
			      const vec2D< Site<T> > & sites,
			      const vec2D< int     > & all_site_labels,
			      const vec2D< int     > & all_site_colours,
			      vec3D< int     > & halo_sending_setup,
			      vec3D< int     > & halo_recving_setup) {

  int pos=0;  
  // loop over all of the sites
  for( const Site<T> &site_i : sites[ which_process ]) {


    // loop over the neighbours of each site
    for (int j : site_i.neighbours() ) { 

      // check if the neighbour is owned by the same process as the site
      if ( binary_search(all_site_labels[which_process].begin(), all_site_labels[which_process].end(), j) ) {
	// if it is, we need no further action - move to the next site
	continue;
      } else {
	// else find out which other process the neighbour is owned by
	for ( int k=0; k<nproc; k++) {

	  if (k == which_process) continue; // don't need halo data with yourself

	  auto it = find(all_site_labels[k].begin(), all_site_labels[k].end(), j);
	  if( it != all_site_labels[k].end() ) {

	    // add site_i.label() to halo_sending [k] on process 'which_process'
	    // add j              to halo_recving [k] on process 'which_process' 

	    halo_sending_setup[k][ site_i.colour() ].push_back( all_site_labels[which_process][pos] );
	    halo_recving_setup[k][ all_site_colours[k][it-all_site_labels[k].begin()]].push_back( j );
		   		  
	    break;		  
	  }
	} // end of k loop
      }	// end of else
    } // end of neighbour loop
    pos++;
  } // end of loop over sites
}

// utility function for graph file constructor
template<typename Update>
void Graph<Update>::send_halo_data(const int send_to_process,
			      const vec3D <int> & halo_sending_setup,
			      const vec3D <int> & halo_recving_setup) {
  
  if( send_to_process == 0 ) {
    this->halo_recving = halo_recving_setup;
    this->halo_sending = halo_sending_setup;

  } else {

    // package data for sending
    int send_sizes[ nproc*this->colours ]; // number of site values sent to each process
    int recv_sizes[ nproc*this->colours ]; // number of site values recved from each process

    int total_send=0, total_recv=0;

    for(int i=0; i<nproc; i++) {
      for(int j=0; j<this->colours; j++) {
	send_sizes[i + nproc*j] = halo_sending_setup[i][j].size();
	recv_sizes[i + nproc*j] = halo_recving_setup[i][j].size();

	total_send += send_sizes[i + nproc*j];
	total_recv += recv_sizes[i + nproc*j];
      }
    }

    std::vector<int> halo_recving_1D(total_recv, 0), halo_sending_1D(total_send, 0);

    int position_recv=0, position_send=0;

    // move the halo data to a 1D array for sending	  
    for(int i=0; i<nproc; i++) {
      for(int j=0; j<this->colours; j++) {
	std::copy( halo_recving_setup[i][j].begin(), halo_recving_setup[i][j].end(), halo_recving_1D.begin() + position_recv );
	std::copy( halo_sending_setup[i][j].begin(), halo_sending_setup[i][j].end(), halo_sending_1D.begin() + position_send );

	position_recv += recv_sizes[i + nproc*j];
	position_send += send_sizes[i + nproc*j];
      }
    }

    // send the data
    MPI_Send( &recv_sizes[0], nproc*this->colours, MPI_INT, send_to_process, 5, MPI_COMM_WORLD); 
    MPI_Send( &send_sizes[0], nproc*this->colours, MPI_INT, send_to_process, 6, MPI_COMM_WORLD); 

    MPI_Send( &halo_recving_1D[0], total_recv, MPI_INT, send_to_process, 7, MPI_COMM_WORLD); 
    MPI_Send( &halo_sending_1D[0], total_send, MPI_INT, send_to_process, 8, MPI_COMM_WORLD); 
  }
}

// utility function for graph file constructor
template<typename Update>
void Graph<Update>::unpack_recv_data(std::vector<int> & site_labels,
				std::vector<int> & site_colours,
				std::vector<int> & num_of_neighbours,
				std::vector<int> & neighbour_labels) {
  MPI_Status stat[ nproc ];
  int info_buffer[3];

  MPI_Recv( info_buffer, 3, MPI_INT, 0, 0, MPI_COMM_WORLD, &stat[process]);

  int num_items = info_buffer[0];
  this->colours = info_buffer[1];
  this->size_global = info_buffer[2];

  site_labels.resize       (num_items, 0);
  site_colours.resize      (num_items, 0);
  num_of_neighbours.resize (num_items, 0);

  MPI_Recv( &site_labels[0],       num_items, MPI_INT, 0, 1, MPI_COMM_WORLD, &stat[ process ]);
  MPI_Recv( &site_colours[0],      num_items, MPI_INT, 0, 2, MPI_COMM_WORLD, &stat[ process ]);
  MPI_Recv( &num_of_neighbours[0], num_items, MPI_INT, 0, 3, MPI_COMM_WORLD, &stat[ process ]);

  int total_neighbours=0;
  for(int j : num_of_neighbours)
    total_neighbours += j;

  neighbour_labels.resize(total_neighbours,0);

  MPI_Recv( &neighbour_labels[0],  total_neighbours, MPI_INT, 0, 4, MPI_COMM_WORLD, &stat[ process ]);

  int in_sizes [nproc*this->colours];
  int out_sizes[nproc*this->colours];
  std::vector<int> halo_recving_1D, halo_sending_1D;

  MPI_Recv( in_sizes,  nproc*this->colours, MPI_INT, 0, 5, MPI_COMM_WORLD, &stat[ process ]);
  MPI_Recv( out_sizes, nproc*this->colours, MPI_INT, 0, 6, MPI_COMM_WORLD, &stat[ process ]);

  // calculate totals                                                                                                                                    
  int total_in=0, total_out=0;
  for(int i=0; i<nproc; i++) {
    for(int j=0; j<this->colours; j++) {
      total_in  += in_sizes [i + j*nproc];
      total_out += out_sizes[i + j*nproc];
    }
  }

  halo_recving_1D.resize(total_in);
  halo_sending_1D.resize(total_out);

  MPI_Recv( &halo_recving_1D[0], total_in,  MPI_INT, 0, 7, MPI_COMM_WORLD, &stat[ process ]);
  MPI_Recv( &halo_sending_1D[0], total_out, MPI_INT, 0, 8, MPI_COMM_WORLD, &stat[ process ]);

  int position_in=0;
  int position_out=0;

  halo_recving.resize(nproc);
  halo_sending.resize(nproc);

  for(int i=0; i<nproc; i++) {
    halo_sending[i].resize(this->colours);
    halo_recving[i].resize(this->colours);
    
    for(int j=0; j<this->colours; j++) {
      halo_recving[i][j].resize(in_sizes [i + nproc*j]);
      halo_sending[i][j].resize(out_sizes[i + nproc*j]);
    
      std::copy(halo_recving_1D.begin() + position_in,  halo_recving_1D.begin() + position_in  + in_sizes [i + nproc*j], this->halo_recving[i][j].begin());
      std::copy(halo_sending_1D.begin() + position_out, halo_sending_1D.begin() + position_out + out_sizes[i + nproc*j], this->halo_sending[i][j].begin());

      position_in  += in_sizes [i + nproc*j];
      position_out += out_sizes[i + nproc*j];
    }
  }
}

// utility function for graph file constructor
template< typename Update >
void Graph<Update>::init_local_sites(const std::vector<int> & site_labels,
				const std::vector<int> & site_colours,
				const std::vector<int> & num_of_neighbours,
				const std::vector<int> & neighbour_labels) {
  int position=0;
  this->local_sites.resize(size_global);
  this->local_owned.resize(colours); // one sub vector for each colour
  this->random_doubles.resize(colours);
  this->size_local = site_labels.size(); // set local size

  for(unsigned int i=0; i < site_labels.size(); i++) {
    std::vector<int> neighbours;
    
    for(int j=0; j<num_of_neighbours[i]; j++) {
      neighbours.push_back( neighbour_labels[ position ] );
      position++;
    }
    
    this->local_sites[ site_labels [i] ] = Site<T> (neighbours, site_colours[i], process);
    this->local_owned[ site_colours[i] ].push_back( site_labels[i] );
  }
  
  // loop over halo_recving
  // do the halo_recving sites have to know what their neighbours are? I hope not...

  for(int i=0; i<nproc; i++) {
    for(int j=0; j<this->colours; j++) {
      for(int k : halo_recving[i][j]) {
	this->local_sites[k] = Site<T> (std::vector<int>(), j, i);
      }
    }
  }

  // size and populate the random_doubles vector
  for(int i=0; i<this->colours; i++) {
    random_doubles[i].resize(local_owned[i].size());

    for(double &j : random_doubles[i]) {
      j = rand_double(mt());
    }
  }
}
