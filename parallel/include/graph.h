/*
  An arbitrary graph class with spins at it's sites, which can be further specialised
  (parallel version)
*/

#ifndef __GRAPH_H__
#define __GRAPH_H__

#include "mersenne.h"
#include "site.h"

#include <algorithm>
#include <exception>
#include <fstream>
#include <iostream>
#include <mpi.h>
#include <random>
#include <sstream>
#include <string>
#include <unistd.h>
#include <vector>

extern int process, nproc;

namespace Parallel {

  template<typename Update>
  class Test;

  template<typename Update>
  class Graph : private Update {

    typedef typename Update::T T;
    using Update::update;
    using Update::initialise_update;

    friend class Test<Update>; // for unit testing
    
    template <typename U>
    using vec2D = std::vector< std::vector<U> >;
    
    template <typename U>
    using vec3D = std::vector< std::vector< std::vector<U> > >;

    int colours;
    int size_global;  // size of the entire graph
    int size_local;   // number of sites owned by this process
  
    vec3D<int> halo_recving;  // the sites on other processes that we recieve data from - first index indicates which rank, second which colour
    vec3D<int> halo_sending; // the sites on this process that we send data from - first index indicates which rank, second which colour

    vec3D<T> halo_values_sending;    // values of the halo data from each site - first index indicates which rank, second which colour
    vec3D<T> halo_values_recving;    // values of the halo data from each site - first index indicates which rank, second which colour

    std::vector< Site<T> > local_sites; // all the sites stored locally (including halo sites)
    vec2D<int> local_owned;   // keep track of the sites to update, as opposed to the halo sites. Different colours stored together

    vec2D<int> send_list; // processes to send data to (per colour)
    vec2D<int> recv_list; // processes to recv data from (per colour)

    vec2D<MPI_Request> send_req;
    vec2D<MPI_Request> recv_req;

    vec2D<MPI_Status> send_stat;
    vec2D<MPI_Status> recv_stat;

    std::uniform_real_distribution<double> rand_double;
    vec2D<double> random_doubles; // to store random numbers for the metropolis update
                                  // store them because we can generate them while we're waiting for the non blocking comms

    // utility functions to make the graph file constructor clearer
    void read_file_data(const std::string & filestub, 
			bool rebalance,
			vec2D< Site<T> > & sites,
			vec2D< int > & all_site_labels,
			vec2D< int > & all_site_colours,
			vec2D< int > & all_num_of_neighbours,
			vec2D< int > & all_neighbour_labels );

    void send_file_data( const int send_to_process, 
			 const vec2D< int > & all_site_labels,
			 const vec2D< int > & all_site_colours,
			 const vec2D< int > & all_num_of_neighbours,
			 const vec2D< int > & all_neighbour_labels);

    void init_halo_data(const int on_which_process,
			const vec2D< Site<T> > & sites,
			const vec2D< int     > & all_site_labels,
			const vec2D< int     > & all_site_colours,
			vec3D <int> & all_halo_sending,
			vec3D <int> & all_halo_recving);

    void send_halo_data(const int send_to_process,
			const vec3D <int> & all_halo_sending,
			const vec3D <int> & all_halo_recving);

    void unpack_recv_data(std::vector<int> & site_labels,
			  std::vector<int> & site_colours,
			  std::vector<int> & num_of_neighbours,
			  std::vector<int> & neighbour_labels);

    void init_local_sites(const std::vector<int> & site_labels,
			  const std::vector<int> & site_colours,
			  const std::vector<int> & num_of_neighbours,
			  const std::vector<int> & neighbour_labels);
    
    typedef typename std::vector<int>::iterator iterator;
    typedef typename std::vector<int>::const_iterator const_iterator;

  public:

    // copy constructor
    Graph(const Graph<Update> &g);

    // file constructor
    Graph(const std::string & filestub, bool rebalance=true);

    // overloaded [] operator to access values of sites
    inline const Site<T> &operator[] (const int x) const { return local_sites.at(x); }
    inline       Site<T> &operator[] (const int x)       { return local_sites[x]; }

    // access properties of the graph and it's sites
    inline const int size          () const { return size_global; }
    inline const int local_size    () const { return size_local;  }
    inline const int total_colours () const { return colours;     }
    
    // communicate changes to other procs
    inline void update_halo(int update_colour) { update(update_colour, local_sites, halo_sending, halo_recving, halo_values_sending, halo_values_recving,
						 send_list, recv_list, send_req, recv_req, send_stat, recv_stat, random_doubles); }
    
    // access the graph's random doubles, filled up during the communications
    inline const double &rand(int colour, int i) { return random_doubles[colour][i]; }

    // iterators over locally owned sites
    inline iterator begin(int colour) { return local_owned[colour].begin(); }
    inline iterator end  (int colour) { return local_owned[colour].end();   }

    // constant iterators over locally owned sites
    inline const_iterator cbegin(int colour) const { return local_owned[colour].cbegin(); }
    inline const_iterator cend  (int colour) const { return local_owned[colour].cend();   }  

    ~Graph() {}
  };

#include "tpp/graph.tpp"

}
#endif
